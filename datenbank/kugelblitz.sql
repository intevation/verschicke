--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-1.pgdg16.04+1)
-- Dumped by pg_dump version 11.2 (Ubuntu 11.2-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: benutzer; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA benutzer;


ALTER SCHEMA benutzer OWNER TO postgres;

--
-- Name: rolle; Type: TYPE; Schema: benutzer; Owner: postgres
--

CREATE TYPE benutzer.rolle AS (
	id integer,
	rollenname character varying,
	kann_einloggen boolean,
	alarm_admin character(1),
	alarm_gebiete character(1),
	alarm_gebietgruppen character(1),
	alarm_teilnehmer character(1),
	alarm_alarme character(1),
	user_admin character(1),
	gruppen_admin character(1)
);


ALTER TYPE benutzer.rolle OWNER TO postgres;

--
-- Name: session; Type: TYPE; Schema: benutzer; Owner: postgres
--

CREATE TYPE benutzer.session AS (
	id integer,
	username character varying,
	start timestamp with time zone,
	stop timestamp with time zone,
	bounds public.box2d,
	max_displayzeit integer,
	archiv_tage integer,
	archiv_ab timestamp with time zone,
	max_zoom integer,
	min_zoom integer,
	loginallowed boolean,
	liveblids boolean,
	animation boolean,
	sound boolean,
	theme_id integer,
	statistik_windowed boolean,
	statistikgebiet boolean
);


ALTER TYPE benutzer.session OWNER TO postgres;

--
-- Name: alarm_userid(integer); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION benutzer.alarm_userid(_userid integer) RETURNS SETOF public.livegebiet
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE
  _v_session  record;
BEGIN

  SELECT home_gruppen_id AS group
  FROM benutzer.benutzer b
  WHERE b.id = _userid
  INTO _v_session;

  IF _v_session IS NULL THEN
    RETURN;
  END IF;

  RETURN QUERY SELECT DISTINCT gebiete.id, gebiete.gebiet,
                               gebiete.gebietgeom,
                               gebiete.blitzanzahl, gebiete.alarm,
                               ( SELECT teilnehmer_aktiv.aktiv
                                   FROM alarmserver.teilnehmer_aktiv
                                   WHERE teilnehmer_aktiv.id
                                         = alarme.teilnehmer) AS aktiv
                 FROM alarmserver.alarme, alarmserver.gebiete
                 WHERE alarme.kunde IN ( SELECT gruppe2alarm.slavekunde
                                           FROM benutzer.gruppe2alarm
                                           WHERE gruppe2alarm.mastergruppe
                                                 = _v_session.group )
                       AND alarme.gebiet = gebiete.id;

  RETURN;
END;
$$;


ALTER FUNCTION benutzer.alarm_userid(_userid integer) OWNER TO postgres;

--
-- Name: alarmgebietlive(integer); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION benutzer.alarmgebietlive(_userid integer) RETURNS SETOF public.livegebiet
    LANGUAGE plpgsql STABLE
    AS $$

DECLARE
  _v_session  record;
BEGIN

  SELECT home_gruppen_id AS group
  FROM benutzer.benutzer b
  WHERE b.id = _userid
  INTO _v_session;

  IF _v_session IS NULL THEN
    RETURN;
  END IF;

  RETURN QUERY SELECT DISTINCT gebiete.id, gebiete.gebiet,
                               gebiete.gebietgeom,
                               gebiete.blitzanzahl, gebiete.alarm,
                               ( SELECT teilnehmer_aktiv.aktiv
                                   FROM alarmserver.teilnehmer_aktiv
                                   WHERE teilnehmer_aktiv.id
                                         = alarme.teilnehmer) AS aktiv
                 FROM alarmserver.alarme, alarmserver.gebiete
                 WHERE alarme.kunde IN ( SELECT gruppe2alarm.slavekunde
                                           FROM benutzer.gruppe2alarm
                                           WHERE gruppe2alarm.mastergruppe
                                                 = _v_session.group )
                       AND alarme.gebiet = gebiete.id;

  RETURN;
END;

$$;


ALTER FUNCTION benutzer.alarmgebietlive(_userid integer) OWNER TO postgres;

--
-- Name: getsession(character varying, character varying); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION benutzer.getsession(ibenutzer character varying, ipasswort character varying) RETURNS SETOF benutzer.session
    LANGUAGE plpgsql IMMUTABLE
    AS $_$
DECLARE
	result benutzer.session ;
	userrecord record;
	userrole int;
BEGIN
	EXECUTE 'SELECT id,home_gruppen_id as hgid from benutzer.benutzer where benutzername = $1' into userrecord using ibenutzer;
	IF userrecord IS NULL THEN
			RETURN;
		END IF;
	userrole := (select * from benutzer.getuserrole(userrecord.id,userrecord.hgid));
	RETURN QUERY SELECT 	benutzer.id, benutzer.benutzername as username, benutzer.startdatum as start,
                                                benutzer.stopdatum as stop, box2d(gruppen.blitzgebiet) as bounds, gruppen.max_displayzeit,
                                                gruppen.archiv_tage, gruppen.archiv_ab, gruppen.max_zoom, gruppen.min_zoom,
                                                rollen.kann_einloggen as LogInAllowed, gruppen.liveblids, gruppen.animation, 
                                                gruppen.sound, gruppen.theme_id, gruppen.statistik_windowed, gruppen.statistikgebiet is not null as statistikgebiet
						FROM benutzer.benutzer, benutzer.gruppen, benutzer.rollen
						WHERE rollen.id = userrole
						and gruppen.id = benutzer.home_gruppen_id
						and benutzer.benutzername = ibenutzer AND benutzer.passwort = md5(ipasswort);
						/*and rollen.kann_einloggen is true;*/
						
END;
$_$;


ALTER FUNCTION benutzer.getsession(ibenutzer character varying, ipasswort character varying) OWNER TO postgres;

--
-- Name: getuserperm(integer, integer); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION benutzer.getuserperm(benutzer integer, gruppe integer) RETURNS SETOF benutzer.rolle
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE
    rolle integer;
    gruppe_id integer;
    result benutzer.rolle;
    child boolean;
    permission character(1);
BEGIN
	gruppe_id := gruppe;
	child := FALSE;
	LOOP
		EXECUTE 'SELECT rollen_id from benutzer.berechtigungen WHERE benutzer_id = ' || benutzer || ' AND gruppen_id = ' || gruppe_id into rolle;
		IF rolle IS not NULL THEN
			EXIT;
		ELSE
			child := TRUE;
			execute 'SELECT vatergruppe from benutzer.gruppen where id = ' || gruppe_id into gruppe_id;
			IF gruppe_id is NULL THEN
				RETURN;
			END IF;
		END IF;
	END LOOP;
	execute 'SELECT gruppen_admin from benutzer.rollen where id = ' || rolle into permission;
	if permission = 'C' then 
		if child then 
			permission = 'A';
		end if;
	end if;
		
	RETURN QUERY SELECT id, rollenname, kann_einloggen, alarm_admin, alarm_gebiete, alarm_gebietgruppen, 
							alarm_teilnehmer, alarm_alarme, user_admin, permission as gruppen_admin 
							from benutzer.rollen where id = rolle;
END;
$$;


ALTER FUNCTION benutzer.getuserperm(benutzer integer, gruppe integer) OWNER TO postgres;

--
-- Name: getuserrole(integer, integer); Type: FUNCTION; Schema: benutzer; Owner: postgres
--

CREATE FUNCTION benutzer.getuserrole(benutzer integer, gruppe integer) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
DECLARE
    result integer;
    gruppe_id integer;
BEGIN
	gruppe_id := gruppe;

	LOOP
		EXECUTE 'SELECT rollen_id from benutzer.berechtigungen WHERE benutzer_id = ' || benutzer || ' AND gruppen_id = ' || gruppe_id into result;
		IF result IS not NULL THEN
			EXIT;
		ELSE
			execute 'SELECT vatergruppe from benutzer.gruppen where id = ' || gruppe_id into gruppe_id;
			IF gruppe_id is NULL THEN
				EXIT;
			END IF;
		END IF;
	END LOOP;
	RETURN result;
END;
$$;


ALTER FUNCTION benutzer.getuserrole(benutzer integer, gruppe integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adminlogging; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.adminlogging (
    id integer NOT NULL,
    benutzer_id integer NOT NULL,
    datum timestamp with time zone DEFAULT now() NOT NULL,
    request character varying NOT NULL,
    sessionid character varying NOT NULL,
    ip character varying(45) DEFAULT '-'::character varying NOT NULL
);


ALTER TABLE benutzer.adminlogging OWNER TO postgres;

--
-- Name: benutzer; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.benutzer (
    id integer NOT NULL,
    benutzername character varying,
    passwort character varying,
    home_gruppen_id integer,
    startdatum timestamp with time zone DEFAULT now() NOT NULL,
    stopdatum timestamp with time zone DEFAULT (now() + '1 year'::interval) NOT NULL,
    firstname character varying,
    lastname character varying,
    company character varying,
    division character varying,
    streetadress character varying,
    zip character varying,
    city character varying,
    phonenumber character varying,
    annotation character varying,
    mobilenumber character varying,
    email character varying
);


ALTER TABLE benutzer.benutzer OWNER TO postgres;

--
-- Name: adminlog; Type: VIEW; Schema: benutzer; Owner: postgres
--

CREATE VIEW benutzer.adminlog AS
 SELECT t1.id,
    t2.benutzername,
    t2.firstname,
    t2.lastname,
    t2.company,
    to_char(t1.datum, 'YYYY-MM-DD HH24:MI:SS'::text) AS datum,
    t1.request,
    t1.sessionid,
    t1.ip
   FROM benutzer.adminlogging t1,
    benutzer.benutzer t2
  WHERE (t2.id = t1.benutzer_id)
  ORDER BY t1.id DESC;


ALTER TABLE benutzer.adminlog OWNER TO postgres;

--
-- Name: adminlogging_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer.adminlogging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer.adminlogging_id_seq OWNER TO postgres;

--
-- Name: adminlogging_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer.adminlogging_id_seq OWNED BY benutzer.adminlogging.id;


--
-- Name: benutzer_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer.benutzer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer.benutzer_id_seq OWNER TO postgres;

--
-- Name: benutzer_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer.benutzer_id_seq OWNED BY benutzer.benutzer.id;


--
-- Name: berechtigungen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.berechtigungen (
    benutzer_id integer NOT NULL,
    gruppen_id integer NOT NULL,
    rollen_id integer
);


ALTER TABLE benutzer.berechtigungen OWNER TO postgres;

--
-- Name: ebenen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.ebenen (
    id integer NOT NULL,
    geoserver_ebene character varying,
    reload boolean DEFAULT false NOT NULL,
    baselayer boolean DEFAULT false NOT NULL,
    blitzlayer boolean DEFAULT false NOT NULL,
    singletile boolean DEFAULT false NOT NULL,
    standardname character varying DEFAULT 'Ebene'::character varying,
    externer_server character varying(200),
    externer_parameter character varying,
    archivlayer boolean DEFAULT false NOT NULL
);


ALTER TABLE benutzer.ebenen OWNER TO postgres;

--
-- Name: ebenen_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer.ebenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer.ebenen_id_seq OWNER TO postgres;

--
-- Name: ebenen_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer.ebenen_id_seq OWNED BY benutzer.ebenen.id;


--
-- Name: gruppe2alarm; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.gruppe2alarm (
    mastergruppe integer NOT NULL,
    slavekunde integer NOT NULL
);


ALTER TABLE benutzer.gruppe2alarm OWNER TO postgres;

--
-- Name: TABLE gruppe2alarm; Type: COMMENT; Schema: benutzer; Owner: postgres
--

COMMENT ON TABLE benutzer.gruppe2alarm IS 'ordnet einer Gruppe Alarmkunden zu.';


--
-- Name: COLUMN gruppe2alarm.mastergruppe; Type: COMMENT; Schema: benutzer; Owner: postgres
--

COMMENT ON COLUMN benutzer.gruppe2alarm.mastergruppe IS 'Der Gruppe, der Alarmkunden zugeordnet werden';


--
-- Name: COLUMN gruppe2alarm.slavekunde; Type: COMMENT; Schema: benutzer; Owner: postgres
--

COMMENT ON COLUMN benutzer.gruppe2alarm.slavekunde IS 'Der Kunder der von der Mastergruppe bearbeitet werden kann';


--
-- Name: gruppen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.gruppen (
    id integer NOT NULL,
    vatergruppe integer,
    gruppenname character varying,
    theme_id integer,
    blitzgebiet public.geometry,
    max_displayzeit integer,
    archiv_tage integer DEFAULT 0 NOT NULL,
    archiv_ab timestamp with time zone,
    max_zoom integer DEFAULT 14 NOT NULL,
    min_zoom integer DEFAULT 6 NOT NULL,
    liveblids boolean DEFAULT false,
    animation boolean DEFAULT false,
    sound boolean DEFAULT false,
    pfad character varying DEFAULT 1 NOT NULL,
    max_alarme integer DEFAULT 1 NOT NULL,
    max_alarmgebiete integer DEFAULT 1 NOT NULL,
    max_alarmteilnehmer integer DEFAULT 1 NOT NULL,
    alarmgebiet public.geometry,
    max_alarmarea integer DEFAULT 30000 NOT NULL,
    statistikgebiet public.geometry,
    statistik_windowed boolean DEFAULT true NOT NULL,
    CONSTRAINT enforce_dims_blitzgebiet CHECK ((public.st_ndims(blitzgebiet) = 2)),
    CONSTRAINT enforce_geotype_blitzgebiet CHECK (((public.geometrytype(blitzgebiet) = 'POLYGON'::text) OR (blitzgebiet IS NULL))),
    CONSTRAINT enforce_srid_blitzgebiet CHECK ((public.st_srid(blitzgebiet) = 4326))
);


ALTER TABLE benutzer.gruppen OWNER TO postgres;

--
-- Name: COLUMN gruppen.max_alarmarea; Type: COMMENT; Schema: benutzer; Owner: postgres
--

COMMENT ON COLUMN benutzer.gruppen.max_alarmarea IS 'Alarmgebiet in km² pro Alarm';


--
-- Name: gruppen_ebenen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.gruppen_ebenen (
    id integer NOT NULL,
    gruppen_id integer,
    ebenen_id integer,
    reload_time integer DEFAULT '-1'::integer NOT NULL,
    anzeige_name character varying DEFAULT 'Layer'::character varying NOT NULL,
    feature_info boolean DEFAULT false NOT NULL,
    checked_onlogin boolean DEFAULT false NOT NULL,
    permanent boolean DEFAULT false NOT NULL,
    single_tile boolean DEFAULT true NOT NULL,
    cql_filter json,
    popup_template character varying
);


ALTER TABLE benutzer.gruppen_ebenen OWNER TO postgres;

--
-- Name: gruppen_ebenen_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer.gruppen_ebenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer.gruppen_ebenen_id_seq OWNER TO postgres;

--
-- Name: gruppen_ebenen_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer.gruppen_ebenen_id_seq OWNED BY benutzer.gruppen_ebenen.id;


--
-- Name: gruppen_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer.gruppen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer.gruppen_id_seq OWNER TO postgres;

--
-- Name: gruppen_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer.gruppen_id_seq OWNED BY benutzer.gruppen.id;


--
-- Name: host_themes; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.host_themes (
    host text NOT NULL,
    theme_id integer NOT NULL
);


ALTER TABLE benutzer.host_themes OWNER TO postgres;

--
-- Name: logging; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.logging (
    id integer NOT NULL,
    benutzer character varying NOT NULL,
    datum timestamp with time zone DEFAULT now() NOT NULL,
    request character varying NOT NULL,
    ip character varying(45) DEFAULT '-'::character varying NOT NULL,
    session character varying
);


ALTER TABLE benutzer.logging OWNER TO postgres;

--
-- Name: logging_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer.logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer.logging_id_seq OWNER TO postgres;

--
-- Name: logging_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer.logging_id_seq OWNED BY benutzer.logging.id;


--
-- Name: resetpw; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.resetpw (
    id integer NOT NULL,
    benutzer integer NOT NULL,
    hash character varying NOT NULL,
    stopdatum timestamp with time zone NOT NULL,
    ip character varying(45) DEFAULT '-'::character varying NOT NULL
);


ALTER TABLE benutzer.resetpw OWNER TO postgres;

--
-- Name: resetPW_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer."resetPW_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer."resetPW_id_seq" OWNER TO postgres;

--
-- Name: resetPW_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer."resetPW_id_seq" OWNED BY benutzer.resetpw.id;


--
-- Name: rollen; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.rollen (
    id integer NOT NULL,
    rollenname character varying,
    kann_einloggen boolean DEFAULT false NOT NULL,
    alarm_admin character(1) DEFAULT 'N'::bpchar NOT NULL,
    alarm_gebiete character(1) DEFAULT 'N'::bpchar NOT NULL,
    alarm_gebietgruppen character(1) DEFAULT 'N'::bpchar NOT NULL,
    alarm_teilnehmer character(1) DEFAULT 'N'::bpchar NOT NULL,
    alarm_alarme character(1) DEFAULT 'N'::bpchar NOT NULL,
    user_admin character(1) DEFAULT 'N'::bpchar NOT NULL,
    gruppen_admin character(1) DEFAULT 'N'::bpchar NOT NULL,
    blids_counter character(1) DEFAULT 'N'::bpchar NOT NULL
);


ALTER TABLE benutzer.rollen OWNER TO postgres;

--
-- Name: rollen_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer.rollen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer.rollen_id_seq OWNER TO postgres;

--
-- Name: rollen_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer.rollen_id_seq OWNED BY benutzer.rollen.id;


--
-- Name: sessions; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.sessions (
    sessionid character varying(32) NOT NULL,
    daten text,
    zeit timestamp with time zone,
    benutzer integer
);


ALTER TABLE benutzer.sessions OWNER TO postgres;

--
-- Name: themes; Type: TABLE; Schema: benutzer; Owner: postgres
--

CREATE TABLE benutzer.themes (
    id integer NOT NULL,
    name character varying NOT NULL,
    header character varying,
    footer character varying,
    style character varying,
    title character varying
);


ALTER TABLE benutzer.themes OWNER TO postgres;

--
-- Name: themes_id_seq; Type: SEQUENCE; Schema: benutzer; Owner: postgres
--

CREATE SEQUENCE benutzer.themes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE benutzer.themes_id_seq OWNER TO postgres;

--
-- Name: themes_id_seq; Type: SEQUENCE OWNED BY; Schema: benutzer; Owner: postgres
--

ALTER SEQUENCE benutzer.themes_id_seq OWNED BY benutzer.themes.id;


--
-- Name: userlog; Type: VIEW; Schema: benutzer; Owner: postgres
--

CREATE VIEW benutzer.userlog AS
 SELECT t1.id,
    t1.benutzer,
    t2.firstname,
    t2.lastname,
    t2.company,
    to_char(t1.datum, 'YYYY-MM-DD HH24:MI:SS'::text) AS datum,
    t1.request,
    t1.ip
   FROM benutzer.logging t1,
    benutzer.benutzer t2
  WHERE ((t1.benutzer)::text = (t2.benutzername)::text)
  ORDER BY t1.id DESC;


ALTER TABLE benutzer.userlog OWNER TO postgres;

--
-- Name: adminlogging id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.adminlogging ALTER COLUMN id SET DEFAULT nextval('benutzer.adminlogging_id_seq'::regclass);


--
-- Name: benutzer id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.benutzer ALTER COLUMN id SET DEFAULT nextval('benutzer.benutzer_id_seq'::regclass);


--
-- Name: ebenen id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.ebenen ALTER COLUMN id SET DEFAULT nextval('benutzer.ebenen_id_seq'::regclass);


--
-- Name: gruppen id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.gruppen ALTER COLUMN id SET DEFAULT nextval('benutzer.gruppen_id_seq'::regclass);


--
-- Name: gruppen_ebenen id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.gruppen_ebenen ALTER COLUMN id SET DEFAULT nextval('benutzer.gruppen_ebenen_id_seq'::regclass);


--
-- Name: logging id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.logging ALTER COLUMN id SET DEFAULT nextval('benutzer.logging_id_seq'::regclass);


--
-- Name: resetpw id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.resetpw ALTER COLUMN id SET DEFAULT nextval('benutzer."resetPW_id_seq"'::regclass);


--
-- Name: rollen id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.rollen ALTER COLUMN id SET DEFAULT nextval('benutzer.rollen_id_seq'::regclass);


--
-- Name: themes id; Type: DEFAULT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.themes ALTER COLUMN id SET DEFAULT nextval('benutzer.themes_id_seq'::regclass);


--
-- Name: adminlogging adminlogging_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.adminlogging
    ADD CONSTRAINT adminlogging_pkey PRIMARY KEY (id);


--
-- Name: benutzer benutzer_benutzername_key; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.benutzer
    ADD CONSTRAINT benutzer_benutzername_key UNIQUE (benutzername);


--
-- Name: benutzer benutzer_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.benutzer
    ADD CONSTRAINT benutzer_pkey PRIMARY KEY (id);


--
-- Name: berechtigungen berechtigungen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.berechtigungen
    ADD CONSTRAINT berechtigungen_pkey PRIMARY KEY (benutzer_id, gruppen_id);


--
-- Name: ebenen ebenen_geoname_key; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.ebenen
    ADD CONSTRAINT ebenen_geoname_key UNIQUE (geoserver_ebene);


--
-- Name: ebenen ebenen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.ebenen
    ADD CONSTRAINT ebenen_pkey PRIMARY KEY (id);


--
-- Name: gruppe2alarm gruppe2alarm_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.gruppe2alarm
    ADD CONSTRAINT gruppe2alarm_pkey PRIMARY KEY (mastergruppe, slavekunde);


--
-- Name: gruppen_ebenen gruppen_ebenen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.gruppen_ebenen
    ADD CONSTRAINT gruppen_ebenen_pkey PRIMARY KEY (id);


--
-- Name: gruppen gruppen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.gruppen
    ADD CONSTRAINT gruppen_pkey PRIMARY KEY (id);


--
-- Name: host_themes host_themes_host_key; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.host_themes
    ADD CONSTRAINT host_themes_host_key UNIQUE (host);


--
-- Name: host_themes host_themes_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.host_themes
    ADD CONSTRAINT host_themes_pkey PRIMARY KEY (host);


--
-- Name: logging logging_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (id);


--
-- Name: resetpw resetPW_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.resetpw
    ADD CONSTRAINT "resetPW_pkey" PRIMARY KEY (id);


--
-- Name: rollen rollen_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.rollen
    ADD CONSTRAINT rollen_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (sessionid);


--
-- Name: themes themes_name_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.themes
    ADD CONSTRAINT themes_name_pkey UNIQUE (name);


--
-- Name: themes themes_pkey; Type: CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.themes
    ADD CONSTRAINT themes_pkey PRIMARY KEY (id);


--
-- Name: host_themes host_themes_theme_id_fkey; Type: FK CONSTRAINT; Schema: benutzer; Owner: postgres
--

ALTER TABLE ONLY benutzer.host_themes
    ADD CONSTRAINT host_themes_theme_id_fkey FOREIGN KEY (theme_id) REFERENCES benutzer.themes(id);


--
-- Name: SCHEMA benutzer; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA benutzer TO geoserver;
GRANT USAGE ON SCHEMA benutzer TO aktuell;
GRANT USAGE ON SCHEMA benutzer TO blidspusher;
GRANT USAGE ON SCHEMA benutzer TO alarmadmin;
GRANT USAGE ON SCHEMA benutzer TO current;


--
-- Name: FUNCTION alarm_userid(_userid integer); Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT ALL ON FUNCTION benutzer.alarm_userid(_userid integer) TO kugelblitz;


--
-- Name: TABLE benutzer; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE benutzer.benutzer TO aktuell;
GRANT SELECT ON TABLE benutzer.benutzer TO blidspusher;
GRANT SELECT ON TABLE benutzer.benutzer TO alarmadmin;
GRANT SELECT ON TABLE benutzer.benutzer TO current;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE benutzer.benutzer TO kugelblitz;


--
-- Name: TABLE berechtigungen; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE benutzer.berechtigungen TO alarmadmin;


--
-- Name: TABLE gruppe2alarm; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE benutzer.gruppe2alarm TO geoserver;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE benutzer.gruppe2alarm TO kugelblitz;


--
-- Name: TABLE gruppen; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE benutzer.gruppen TO aktuell;
GRANT SELECT ON TABLE benutzer.gruppen TO blidspusher;
GRANT SELECT ON TABLE benutzer.gruppen TO alarmadmin;


--
-- Name: TABLE rollen; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE benutzer.rollen TO alarmadmin;


--
-- Name: TABLE sessions; Type: ACL; Schema: benutzer; Owner: postgres
--

GRANT SELECT ON TABLE benutzer.sessions TO aktuell;
GRANT SELECT ON TABLE benutzer.sessions TO geoserver;
GRANT SELECT,UPDATE ON TABLE benutzer.sessions TO current;
GRANT SELECT ON TABLE benutzer.sessions TO blidspusher;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE benutzer.sessions TO kugelblitz;


--
-- PostgreSQL database dump complete
--

