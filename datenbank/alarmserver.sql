--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-1.pgdg16.04+1)
-- Dumped by pg_dump version 11.2 (Ubuntu 11.2-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: alarmserver; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA alarmserver;


ALTER SCHEMA alarmserver OWNER TO postgres;

--
-- Name: alarmtyp; Type: TYPE; Schema: alarmserver; Owner: postgres
--

CREATE TYPE alarmserver.alarmtyp AS ENUM (
    'Alarm',
    'Entwarnung',
    'Vorwarnung',
    'Testalarm',
    'Testentwarnung',
    'Testvorwarnung'
);


ALTER TYPE alarmserver.alarmtyp OWNER TO postgres;

--
-- Name: conf_option; Type: TYPE; Schema: alarmserver; Owner: postgres
--

CREATE TYPE alarmserver.conf_option AS ENUM (
    'Debug',
    'SmtpHost',
    'SmtpPort',
    'SmtpHelo',
    'SmtpUser',
    'SmtpPasswd',
    'MailFrom',
    'EcallUser',
    'EcallPasswd',
    'EcallVoiceFrom',
    'EcallFaxFrom',
    'MQTTHost',
    'MQTTPort',
    'MQTTUsername',
    'MQTTPassword',
    'MQTTUseSSL',
    'MQTTCertPath'
);


ALTER TYPE alarmserver.conf_option OWNER TO postgres;

--
-- Name: send_state; Type: TYPE; Schema: alarmserver; Owner: postgres
--

CREATE TYPE alarmserver.send_state AS ENUM (
    'ignored',
    'success',
    'partial success',
    'error'
);


ALTER TYPE alarmserver.send_state OWNER TO postgres;

--
-- Name: alarmgebietlive(character varying); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.alarmgebietlive(session_key character varying) RETURNS SETOF public.livegebiet
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE 

       sql          varchar;
       ergebnis  livegebiet;
 v_session  record;
BEGIN

  EXECUTE 'select home_gruppen_id as group from benutzer.benutzer t1, benutzer.sessions t2 where 
     t2.sessionid =  ' || quote_literal(session_key) || '  and t2.benutzer = t1.id' into v_session;
                
  IF v_session IS NULL THEN
   RETURN;
  END IF;

     sql := ' SELECT distinct gebiete.id, gebiete.gebiet, gebiete.gebietgeom, gebiete.blitzanzahl, gebiete.alarm, 
       ( SELECT teilnehmer_aktiv.aktiv
       FROM alarmserver.teilnehmer_aktiv
      WHERE teilnehmer_aktiv.id = alarme.teilnehmer) AS aktiv
   FROM alarmserver.alarme, alarmserver.gebiete
   WHERE (alarme.kunde IN  
    ( SELECT gruppe2alarm.slavekunde
     FROM benutzer.gruppe2alarm
     WHERE gruppe2alarm.mastergruppe = '|| v_session.group::integer  || ' 
   )) AND alarme.gebiet = gebiete.id;';

 FOR ergebnis IN EXECUTE sql LOOP
  RETURN NEXT ergebnis;
 END LOOP;
END;
$$;


ALTER FUNCTION alarmserver.alarmgebietlive(session_key character varying) OWNER TO postgres;

--
-- Name: expire_alarms(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.expire_alarms() RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  -- Reset stroke counter an alamr status for all areas where no new
  -- strokes were registered for the max. alert time (`alarmdauer'):
  UPDATE alarmserver.gebiete SET blitzanzahl = 0, alarm = FALSE
    WHERE EXTRACT(EPOCH FROM now() - letzterblitz)::int/60 > alarmdauer
          AND blitzanzahl > 0;
  -- Reset alarms in all goups with no alarms in there areas left:
  UPDATE alarmserver.gebietgruppen SET alarm = FALSE
    WHERE alarm AND id NOT IN ( SELECT DISTINCT g2g.gruppe
                                  FROM alarmserver.gebiet2gruppe g2g,
                                       alarmserver.gebiete g
                                  WHERE g2g.gebiet = g.id AND g.alarm );
END;
$$;


ALTER FUNCTION alarmserver.expire_alarms() OWNER TO postgres;

--
-- Name: fill_template(text, alarmserver.alarmtyp, timestamp with time zone, integer, integer, boolean); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.fill_template(_templ text, _typ alarmserver.alarmtyp, _eintrag timestamp with time zone, _gebiet_id integer, _gruppe_id integer, utc boolean) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
 __Deci_Fmt CONSTANT text := '990.000';
 __Date_Fmt CONSTANT text := 'DD.MM.YYYY';
 __Time_Fmt CONSTANT text := 'HH24:MI:SS';
 _gebiet_r         record;
 _gebiet_gruppen_r record;
 _laenge   text;
 _breite   text;
 _gebiet   text;
 _gruppe   text;
 _radius   text;
 _schwelle text;
 _anzahl   text;
 _dichte   text;
 _minuten  text;
BEGIN
  IF utc THEN
    SET TIME ZONE 'UTC';
  ELSE
    SET TIME ZONE 'CET';
  END IF;

  IF _gebiet_id IS NOT NULL THEN
    SELECT gebiet, st_x(st_centroid(gebietgeom)) AS centerx,
           st_y(st_centroid(gebietgeom)) AS centery, radius,
           st_area(st_transform(gebietgeom,31467)) AS flaeche,
           blitzanzahl, blitzezumalarm, alarmdauer
      INTO _gebiet_r
      FROM alarmserver.gebiete WHERE id = _gebiet_id;
  ELSIF _gruppe_id IS NOT NULL THEN
    SELECT gebiet, st_x(st_centroid(gebietgeom)) AS centerx,
           st_y(st_centroid(gebietgeom)) AS centery, radius,
           st_area(st_transform(gebietgeom,31467)) AS flaeche,
           blitzanzahl, blitzezumalarm, alarmdauer
      INTO _gebiet_r
      FROM alarmserver.gebiete
      WHERE id = (SELECT area_triggering_alarm
                    FROM alarmserver.gebietgruppen
                    WHERE id = _gruppe_id);
    SELECT gruppenname INTO _gebiet_gruppen_r
      FROM alarmserver.gebietgruppen WHERE id = _gruppe_id;
    _gruppe := _gebiet_gruppen_r.gruppenname;
  END IF;

  _laenge := to_char(COALESCE(_gebiet_r.centerx, 0.0),
                     __Deci_Fmt);
  _breite := to_char(COALESCE(_gebiet_r.centery, 0.0),
                     __Deci_Fmt);
  _gebiet := _gebiet_r.gebiet;
  _radius := _gebiet_r.radius::text;
  _schwelle := _gebiet_r.blitzezumalarm::text;
  -- As the alarm is triggered _befor_ the latest stoke is inserted
  -- into "alarmserver.gebiete" we have to add one to "blitzanzahl".
  _anzahl := (_gebiet_r.blitzanzahl + 1)::text;
  _dichte := ((_gebiet_r.blitzanzahl + 1) * 10000 / _gebiet_r.flaeche)::text;
  _minuten := _gebiet_r.alarmdauer::text;


  _templ := replace(_templ, '%TYP', _typ::text);
  _templ := replace(_templ, '%DATUM', to_char(_eintrag, __Date_Fmt));
  _templ := replace(_templ, '%ZEIT', to_char(_eintrag, __Time_Fmt));
  _templ := replace(_templ, '%LÄNGE', COALESCE(_laenge, ''));
  _templ := replace(_templ, '%BREITE', COALESCE(_breite, ''));
  _templ := replace(_templ, '%GEBIET', COALESCE(_gebiet, ''));
  _templ := replace(_templ, '%GRUPPE', COALESCE(_gruppe, ''));
  _templ := replace(_templ, '%RADIUS', COALESCE(_radius, ''));
  _templ := replace(_templ, '%SCHWELLE', COALESCE(_schwelle, ''));
  _templ := replace(_templ, '%ANZAHL', COALESCE(_anzahl, ''));
  _templ := replace(_templ, '%DICHTE', COALESCE(_dichte, ''));
  _templ := replace(_templ, '%MINUTEN', COALESCE(_minuten, ''));
  RETURN _templ;
END;
$$;


ALTER FUNCTION alarmserver.fill_template(_templ text, _typ alarmserver.alarmtyp, _eintrag timestamp with time zone, _gebiet_id integer, _gruppe_id integer, utc boolean) OWNER TO postgres;

--
-- Name: gebietalarm(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.gebietalarm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _alarmrec RECORD;
BEGIN
  -- Wenn Alarmierung erreicht
  IF ( NEW.blitzanzahl >= OLD.blitzezumalarm AND OLD.alarm IS false ) THEN
     NEW.alarm := true;
     -- Alle Gruppen, die das Gebiet enthalten auf Alarm setzen
     UPDATE alarmserver.gebietgruppen
       SET alarm = true, area_triggering_alarm = NEW.id
       WHERE alarm IS NOT TRUE
         AND id IN (SELECT DISTINCT gruppe
                      FROM alarmserver.gebiet2gruppe
                      WHERE gebiet = NEW.id);
     -- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
     FOR _alarmrec IN ( SELECT * FROM alarmserver.alarme
                          WHERE gebiet = NEW.id )
     LOOP
       INSERT INTO alarmserver.meldungen (id, alarm, typ)
         VALUES (DEFAULT, _alarmrec.id ,'Alarm');
     END LOOP;
  -- Wenn Entwarnung aus Programm kommt
  ELSEIF ( OLD.alarm IS true AND NEW.alarm IS false ) THEN
    -- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
    FOR _alarmrec IN ( SELECT * FROM alarmserver.alarme
                        WHERE gebiet = NEW.id )
    LOOP
      INSERT INTO alarmserver.meldungen (id, alarm, typ)
        VALUES (DEFAULT, _alarmrec.id,'Entwarnung');
    END LOOP;
  END IF;
  RETURN NEW;
END;
$$;


ALTER FUNCTION alarmserver.gebietalarm() OWNER TO postgres;

--
-- Name: get_alarm_geom(integer); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.get_alarm_geom(_id integer) RETURNS text
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _alarm_r   record;
  _gebiet_id int;
  _gebiet_r  record;
BEGIN

  SELECT a.* INTO _alarm_r
    FROM alarmserver.meldungen m,
         alarmserver.alarme a
    WHERE m.id = _id AND a.id = m.alarm;

  IF _alarm_r.gebiet IS NOT NULL THEN
    _gebiet_id = _alarm_r.gebiet;
  ELSE
    SELECT area_triggering_alarm INTO _gebiet_id
      FROM alarmserver.gebietgruppen
      WHERE id = _alarm_r.gruppe;
  END IF;

  SELECT gebiet AS name,
         blitzanzahl AS count,
         st_astext(gebietgeom) AS geom
    INTO _gebiet_r
    FROM alarmserver.gebiete
    WHERE id = _gebiet_id;

  RETURN row_to_json(_gebiet_r);
END;
$$;


ALTER FUNCTION alarmserver.get_alarm_geom(_id integer) OWNER TO postgres;

--
-- Name: get_alarm_msg(integer); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE OR REPLACE FUNCTION alarmserver.get_alarm_msg(_id integer) RETURNS text
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _msg_data     jsonb;
  _subject      text;
  _body_templ   text;
  _message_r    record;
  _recipient_r  record;
  _alarm_r      record;
  _area_wkt     text;
BEGIN

  SELECT * INTO _message_r FROM alarmserver.meldungen
    WHERE id = _id;
  SELECT * INTO _alarm_r FROM alarmserver.alarme
    WHERE id = _message_r.alarm;
  SELECT * INTO _recipient_r FROM alarmserver.teilnehmer
    WHERE id = _alarm_r.teilnehmer;

  _msg_data := json_build_object ('recipient', _recipient_r.teilnehmer,
                                  'logonly', _recipient_r.nurlog);

  -- the following code is ugly, hints on how to make it less redundant
  -- are highly appreciated... <wilde@intevation.de>
  IF _recipient_r.sms <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('sms', _recipient_r.sms)::jsonb;
  END IF;
  IF _recipient_r.email <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('email', _recipient_r.email)::jsonb;
  END IF;
  IF _recipient_r.tel <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('phone', _recipient_r.tel)::jsonb;
  END IF;
  IF _recipient_r.fax <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('fax', _recipient_r.fax)::jsonb;
  END IF;
  IF _recipient_r.mqtt <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('mqtt', _recipient_r.mqtt)::jsonb;
  END IF;
  IF _recipient_r.mailfrom <> '' THEN
    _msg_data := _msg_data
                || json_build_object ('mailfrom', _recipient_r.mailfrom)::jsonb;
  END IF;

  IF _message_r.typ IN ('Alarm', 'Testalarm') THEN
    _subject := _recipient_r.betreffalarm;
    _body_templ := _recipient_r.alarmierung;
  ELSE
  -- FIXME: 'Vorwarnung' is not really handled.
  --        The legacy java code didn't handle it neither.
    _subject := _recipient_r.betreffentwarn;
    _body_templ := _recipient_r.entwarnung;
  END IF;
  _msg_data := _msg_data
    || json_build_object ('subject', _subject,
                          'body',
                          alarmserver.fill_template(_body_templ,
                                                    _message_r.typ,
                                                    _message_r.eintrag,
                                                    _alarm_r.gebiet,
                                                    _alarm_r.gruppe,
                                                    _recipient_r.utc)
                         )::jsonb;
  RETURN _msg_data::text;
END;
$$;


ALTER FUNCTION alarmserver.get_alarm_msg(_id integer) OWNER TO postgres;

--
-- Name: gruppealarm(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.gruppealarm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
	alarmrec RECORD;
	gruppe int;
	sql varchar;
BEGIN
	gruppe := NEW.id;
	-- Wenn Alarmierung erreicht
	IF ( OLD.alarm is false and NEW.alarm is true) THEN
		-- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
		sql := 'select * from alarmserver.alarme where gruppe = ' || gruppe;
		FOR alarmrec IN EXECUTE sql LOOP
			EXECUTE 'INSERT INTO alarmserver.meldungen (id, alarm, typ) values (DEFAULT,' || alarmrec.id || ',''Alarm'')';
		END LOOP;
	-- Wenn Entwarnung aus Programm kommt
	ELSEIF (OLD.alarm is true and NEW.alarm is false) THEN
		-- Für alle Alarme für dieses Gebiet eine Meldung erzeugen
		sql := 'select * from alarmserver.alarme where gruppe = ' || gruppe;
		FOR alarmrec IN EXECUTE sql LOOP
			EXECUTE 'INSERT INTO alarmserver.meldungen (id, alarm, typ) values (DEFAULT,' || alarmrec.id || ',''Entwarnung'')';
		END LOOP;
	END IF;
        RETURN NEW;
END;
$$;


ALTER FUNCTION alarmserver.gruppealarm() OWNER TO postgres;

--
-- Name: insert_stroke(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.insert_stroke() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _area  record;
BEGIN
  FOR _area IN (
      SELECT gebiete.id, NEW.zeit
              FROM alarmserver.gebiete
              WHERE st_within(NEW.koordinate, gebiete.gebietgeom)
  )
  LOOP
    UPDATE alarmserver.gebiete
       SET letzterblitz = _area.zeit,
           blitzanzahl = blitzanzahl + 1
     WHERE id = _area.id;
  END LOOP;
  RETURN NEW;
END;
$$;


ALTER FUNCTION alarmserver.insert_stroke() OWNER TO postgres;

--
-- Name: notify_alarm(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.notify_alarm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _matching_dow bool;
  _msg_data     jsonb;
  _alarm_r      record;
  _recipient_r  record;
  _startdate    timestamp with time zone;
  _starttime    timestamp with time zone;
  _stopdate     timestamp with time zone;
  _stoptime     timestamp with time zone;
BEGIN
  SELECT * INTO _alarm_r FROM alarmserver.alarme
    WHERE id = NEW.alarm;
  SELECT * INTO _recipient_r FROM alarmserver.teilnehmer
    WHERE id = _alarm_r.teilnehmer;

  -- Reimplement the date time logic of the legacy system:
  -- Only supports UTC and CET via a flag at the subscription.
  -- FIXME: We should change the schema to use timestamp with
  --        tz everywhere and use it to safe the actually
  --        intended time zone.  That would give us Flexibility
  --        and correct handling of DST.
  IF _recipient_r.utc THEN
    _startdate := _recipient_r.startdatum::timestamp AT TIME ZONE 'UTC';
    _stopdate  := _recipient_r.stopdatum::timestamp AT TIME ZONE 'UTC';
    _starttime := (_recipient_r.startzeit
                  + NEW.eintrag::date)::timestamp AT TIME ZONE 'UTC';
    _stoptime  := (_recipient_r.stopzeit
                  + NEW.eintrag::date)::timestamp AT TIME ZONE 'UTC';
  ELSE
    _startdate := _recipient_r.startdatum::timestamp AT TIME ZONE 'CET';
    _stopdate  := _recipient_r.stopdatum::timestamp AT TIME ZONE 'CET';
    _starttime := (_recipient_r.startzeit
                  + NEW.eintrag::date)::timestamp AT TIME ZONE 'CET';
    _stoptime  := (_recipient_r.stopzeit
                  + NEW.eintrag::date)::timestamp AT TIME ZONE 'CET';
  END IF;

  -- check for matching week day:
  EXECUTE 'SELECT '
    || alarmserver.wochentag(to_char(now(),'YYYY-MM-DD'))
    || ' FROM alarmserver.teilnehmer' INTO _matching_dow;

  -- Check if alarm message is still open.
  -- This is so that other triggers could process messages before us.
  IF NEW.erledigt IS NULL THEN
    IF NEW.eintrag < _startdate     -- check subscription period
       OR NEW.eintrag > (_stopdate + interval '1' day)
       OR NEW.eintrag < _starttime     -- check time of day
       OR NEW.eintrag > _stoptime
       OR NOT _matching_dow            -- check day of week
       OR (NEW.typ IN ('Alarm', 'Testalarm')  -- check message text
           AND _recipient_r.alarmierung <> '' IS NOT TRUE)
       OR (NEW.typ IN ('Entwarnung', 'Testentwarnung')
           AND _recipient_r.entwarnung <> '' IS NOT TRUE)
    THEN
      NEW.erledigt := now();
      NEW.status := 'ignored';
      NEW.status_info := 'Notification not desired by recipient.';
    ELSE
      _msg_data := json_build_object ('id', NEW.id,
                                      'groupids',
                                      (SELECT array_to_json(array_agg(mastergruppe))
                                         FROM benutzer.gruppe2alarm
                                         WHERE slavekunde = _alarm_r.kunde));

      _msg_data := json_build_object ('type', NEW.typ,
                                      'data', _msg_data);
      PERFORM pg_notify('blids', _msg_data::text);
    END IF;
  END IF;
  RETURN NEW;
END;
$$;


ALTER FUNCTION alarmserver.notify_alarm() OWNER TO postgres;

--
-- Name: notify_stroke(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.notify_stroke() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
 _data jsonb;
BEGIN
  _data := row_to_json(NEW)::jsonb - 'koordinate'
           || json_build_object ('koordinate',
                                st_astext(NEW.koordinate))::jsonb;
  PERFORM pg_notify('blids',
                    json_build_object ('type', 'stroke',
                                       'data', _data)::text);
  RETURN NEW;
END;
$$;


ALTER FUNCTION alarmserver.notify_stroke() OWNER TO postgres;

--
-- Name: trigger_messages(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.trigger_messages() RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  -- Maybe the thresholds should be configurable, but constants should
  -- be good enough for now:
  --
  -- Time after which a not processed message is triggered again
  -- (in minutes):
  __retrigger_threshold CONSTANT numeric := 5;
  -- Time after which a not processed message is unconditionally set
  -- to error (in minutes):
  __ditch_threshold CONSTANT numeric := 120;

  _msg      record;
  _msg_data jsonb;
BEGIN
  FOR _msg IN (
    SELECT id, eintrag, typ FROM alarmserver.meldungen
      WHERE erledigt IS NULL
        AND EXTRACT(EPOCH FROM now() - eintrag)::int/60
            > __retrigger_threshold
        )
  LOOP
     -- Set everything non processed message older than
     -- __ditch_thrshold to error.
     --
     -- There are numerouse improved heuristics possible, but they
     -- seem not worth the extra complexity, as this really shouldn't
     -- happen at all... -- sw
    IF EXTRACT(EPOCH FROM now() - _msg.eintrag)::int/60
       > __ditch_threshold
    THEN
      UPDATE alarmserver.meldungen
        SET erledigt = now(),
            status = 'error',
            status_info = 'Message wasn''t processed in time'
        WHERE id = _msg.id;
      ELSE
        -- Msg is still young enough, re-trigger it.
        --
        -- The groupid is intentionally left blank in the
        -- notification: The groupid is only for the realtime
        -- visualisation, to determine if it might be shown.  The
        -- re-triggered alarms are not supposed to be shown so we
        -- leave the groupid out.
        _msg_data := json_build_object ('id', _msg.id,
                                        'groupids', null);

        _msg_data := json_build_object ('type', _msg.typ,
                                        'data', _msg_data);
        PERFORM pg_notify('blids', _msg_data::text);
    END IF;
  END LOOP;
END;
$$;


ALTER FUNCTION alarmserver.trigger_messages() OWNER TO postgres;

--
-- Name: update_surface_area(); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.update_surface_area() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  NEW.flaeche = st_area(st_transform(NEW.gebietgeom, 31467));
  RETURN NEW;
END;
$$;


ALTER FUNCTION alarmserver.update_surface_area() OWNER TO postgres;

--
-- Name: wochentag(character varying); Type: FUNCTION; Schema: alarmserver; Owner: postgres
--

CREATE FUNCTION alarmserver.wochentag(zeit character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE 
	wochentag varchar;
	sql varchar;
	ergebnis int;
BEGIN
	sql := 'Select extract (isodow from timestamp ' || quote_literal(zeit) || ')';
        EXECUTE sql INTO ergebnis;
	IF (ergebnis = 1) THEN
       		wochentag := 'mo';
       	ELSEIF (ergebnis = 2) THEN
       		wochentag := 'di';
       	ELSEIF (ergebnis = 3) THEN
       		wochentag := 'mi';
       	ELSEIF (ergebnis = 4) THEN
       		wochentag := 'don';
       	ELSEIF (ergebnis = 5) THEN
       		wochentag := 'fr';
       	ELSEIF (ergebnis = 6) THEN
       		wochentag := 'sa';
       	ELSE 
       		wochentag := 'so';
       	END IF;
	RETURN wochentag;
END;
$$;


ALTER FUNCTION alarmserver.wochentag(zeit character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adminlogins; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.adminlogins (
    id integer NOT NULL,
    kunde integer,
    zeit timestamp with time zone DEFAULT now(),
    anmeldetyp character varying
);


ALTER TABLE alarmserver.adminlogins OWNER TO postgres;

--
-- Name: adminlogins_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarmserver.adminlogins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarmserver.adminlogins_id_seq OWNER TO postgres;

--
-- Name: adminlogins_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarmserver.adminlogins_id_seq OWNED BY alarmserver.adminlogins.id;


--
-- Name: alarme; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.alarme (
    id integer NOT NULL,
    gebiet integer,
    teilnehmer integer,
    kunde integer,
    gruppe integer
);


ALTER TABLE alarmserver.alarme OWNER TO postgres;

--
-- Name: alarme_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarmserver.alarme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarmserver.alarme_id_seq OWNER TO postgres;

--
-- Name: alarme_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarmserver.alarme_id_seq OWNED BY alarmserver.alarme.id;


--
-- Name: kunden; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.kunden (
    id integer NOT NULL,
    kunde character varying,
    benutzer character varying,
    passwort character varying,
    gebiete boolean DEFAULT false,
    teilnehmer boolean DEFAULT false,
    gruppen boolean DEFAULT false,
    alarme boolean DEFAULT true
);


ALTER TABLE alarmserver.kunden OWNER TO postgres;

--
-- Name: anmeldungen; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW alarmserver.anmeldungen AS
 SELECT kunden.kunde,
    to_char(adminlogins.zeit, 'DD.MM.YYYY HH24:MI:SS'::text) AS anmeldezeit,
    adminlogins.anmeldetyp,
    adminlogins.zeit
   FROM alarmserver.adminlogins,
    alarmserver.kunden
  WHERE (kunden.id = adminlogins.kunde)
  ORDER BY adminlogins.zeit DESC;


ALTER TABLE alarmserver.anmeldungen OWNER TO postgres;

--
-- Name: config; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.config (
    option alarmserver.conf_option NOT NULL,
    string character varying,
    "int" integer,
    bool boolean,
    CONSTRAINT config_check CHECK ((((
CASE
    WHEN (string IS NULL) THEN 0
    ELSE 1
END +
CASE
    WHEN ("int" IS NULL) THEN 0
    ELSE 1
END) +
CASE
    WHEN (bool IS NULL) THEN 0
    ELSE 1
END) = 1))
);


ALTER TABLE alarmserver.config OWNER TO postgres;

--
-- Name: gebiet2gruppe; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.gebiet2gruppe (
    id integer NOT NULL,
    gruppe integer,
    gebiet integer
);


ALTER TABLE alarmserver.gebiet2gruppe OWNER TO postgres;

--
-- Name: gebiet2gruppe_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarmserver.gebiet2gruppe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarmserver.gebiet2gruppe_id_seq OWNER TO postgres;

--
-- Name: gebiet2gruppe_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarmserver.gebiet2gruppe_id_seq OWNED BY alarmserver.gebiet2gruppe.id;


--
-- Name: gebiete; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.gebiete (
    id integer NOT NULL,
    gebietgeom public.geometry(MultiPolygon,4326),
    gebiet character varying,
    kunde integer,
    letzterblitz timestamp with time zone,
    blitzezumalarm integer,
    alarmdauer integer,
    blitzanzahl integer DEFAULT 0,
    alarm boolean DEFAULT false,
    radius numeric,
    zentrum public.geometry(Point,4326),
    icon character varying,
    flaeche numeric DEFAULT 0,
    innerradius numeric
);


ALTER TABLE alarmserver.gebiete OWNER TO postgres;

--
-- Name: COLUMN gebiete.flaeche; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN alarmserver.gebiete.flaeche IS 'Flaeche in km²';


--
-- Name: gebiete_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarmserver.gebiete_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarmserver.gebiete_id_seq OWNER TO postgres;

--
-- Name: gebiete_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarmserver.gebiete_id_seq OWNED BY alarmserver.gebiete.id;


--
-- Name: gebietgruppen; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.gebietgruppen (
    id integer NOT NULL,
    gruppenname character varying,
    alarm boolean,
    kunde integer,
    area_triggering_alarm integer
);


ALTER TABLE alarmserver.gebietgruppen OWNER TO postgres;

--
-- Name: gebietgruppen_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarmserver.gebietgruppen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarmserver.gebietgruppen_id_seq OWNER TO postgres;

--
-- Name: gebietgruppen_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarmserver.gebietgruppen_id_seq OWNED BY alarmserver.gebietgruppen.id;


--
-- Name: kunden_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarmserver.kunden_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarmserver.kunden_id_seq OWNER TO postgres;

--
-- Name: kunden_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarmserver.kunden_id_seq OWNED BY alarmserver.kunden.id;


--
-- Name: kundengruppen; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.kundengruppen (
    masterkunde integer NOT NULL,
    slavekunde integer NOT NULL
);


ALTER TABLE alarmserver.kundengruppen OWNER TO postgres;

--
-- Name: TABLE kundengruppen; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON TABLE alarmserver.kundengruppen IS 'ordnet einem Masterkunden untergeordnete Slavekunden zu.';


--
-- Name: COLUMN kundengruppen.masterkunde; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN alarmserver.kundengruppen.masterkunde IS 'Der Kunde der den Slavekunden bearbeiten kann';


--
-- Name: COLUMN kundengruppen.slavekunde; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN alarmserver.kundengruppen.slavekunde IS 'Der Kunder der vm Masterkunden bearbeitet werden kann';


--
-- Name: meldungen; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.meldungen (
    id integer NOT NULL,
    alarm integer,
    eintrag timestamp with time zone DEFAULT now(),
    erledigt timestamp with time zone,
    typ alarmserver.alarmtyp,
    status alarmserver.send_state,
    status_info text
);


ALTER TABLE alarmserver.meldungen OWNER TO postgres;

--
-- Name: meldungen_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarmserver.meldungen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alarmserver.meldungen_id_seq OWNER TO postgres;

--
-- Name: meldungen_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarmserver.meldungen_id_seq OWNED BY alarmserver.meldungen.id;


--
-- Name: tcom_consumer; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW alarmserver.tcom_consumer AS
 SELECT gebiete.gebiet,
    gebiete.gebietgeom,
    gebiete.blitzanzahl,
    gebiete.alarm
   FROM alarmserver.gebiete
  WHERE ((gebiete.kunde = 112) AND ((gebiete.gebiet)::text ~~ 'FS%C%'::text));


ALTER TABLE alarmserver.tcom_consumer OWNER TO postgres;

--
-- Name: tcom_network; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW alarmserver.tcom_network AS
 SELECT gebiete.gebiet,
    gebiete.gebietgeom,
    gebiete.blitzanzahl,
    gebiete.alarm
   FROM alarmserver.gebiete
  WHERE ((gebiete.kunde = 112) AND ((gebiete.gebiet)::text ~~ 'FS%Network%'::text));


ALTER TABLE alarmserver.tcom_network OWNER TO postgres;

--
-- Name: tcom_onkz; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW alarmserver.tcom_onkz AS
 SELECT gebiete.gebiet,
    gebiete.gebietgeom,
    gebiete.blitzanzahl,
    gebiete.alarm
   FROM alarmserver.gebiete
  WHERE ((gebiete.kunde = 112) AND ((gebiete.gebiet)::text !~~ 'FS%'::text) AND ((gebiete.gebiet)::text !~~ '%ATS%'::text));


ALTER TABLE alarmserver.tcom_onkz OWNER TO postgres;

--
-- Name: tcom_onkz_num; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW alarmserver.tcom_onkz_num AS
 SELECT gebiete.gebiet,
    "substring"((gebiete.gebiet)::text, '[0-9]*'::text) AS onkz,
    gebiete.gebietgeom,
    gebiete.blitzanzahl,
    gebiete.alarm
   FROM alarmserver.gebiete
  WHERE ((gebiete.kunde = 112) AND ((gebiete.gebiet)::text !~~ 'FS%'::text) AND ((gebiete.gebiet)::text !~~ '%ATS%'::text));


ALTER TABLE alarmserver.tcom_onkz_num OWNER TO postgres;

--
-- Name: teilnehmer; Type: TABLE; Schema: alarmserver; Owner: postgres
--

CREATE TABLE alarmserver.teilnehmer (
    id integer NOT NULL,
    teilnehmer character varying,
    sms character varying,
    email character varying,
    tel character varying,
    fax character varying,
    alarmierung text,
    entwarnung text,
    kunde integer,
    startdatum timestamp with time zone,
    stopdatum timestamp with time zone,
    startzeit time without time zone DEFAULT '00:00:00'::time without time zone,
    stopzeit time without time zone DEFAULT '24:00:00'::time without time zone,
    mo boolean DEFAULT true,
    di boolean DEFAULT true,
    mi boolean DEFAULT true,
    don boolean DEFAULT true,
    fr boolean DEFAULT true,
    sa boolean DEFAULT true,
    so boolean DEFAULT true,
    nurlog boolean DEFAULT false,
    utc boolean DEFAULT false,
    betreffalarm character varying DEFAULT 'BLIDS Alarm'::character varying,
    betreffentwarn character varying DEFAULT 'BLIDS Entwarnung'::character varying,
    blitzprotokoll boolean DEFAULT false,
    blitzascsv boolean DEFAULT false,
    mailfrom text,
    mqtt character varying
);


ALTER TABLE alarmserver.teilnehmer OWNER TO postgres;

--
-- Name: COLUMN teilnehmer.stopdatum; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON COLUMN alarmserver.teilnehmer.stopdatum IS 'Uhrzeit wird nicht beachtet';


--
-- Name: teilnehmer_aktiv; Type: VIEW; Schema: alarmserver; Owner: postgres
--

CREATE VIEW alarmserver.teilnehmer_aktiv AS
 SELECT teilnehmer.id,
    teilnehmer.teilnehmer,
    teilnehmer.sms,
    teilnehmer.email,
    teilnehmer.tel,
    teilnehmer.fax,
    teilnehmer.alarmierung,
    teilnehmer.entwarnung,
    teilnehmer.kunde,
    teilnehmer.startdatum,
    teilnehmer.stopdatum,
    teilnehmer.startzeit,
    teilnehmer.stopzeit,
    teilnehmer.mo,
    teilnehmer.di,
    teilnehmer.mi,
    teilnehmer.don,
    teilnehmer.fr,
    teilnehmer.sa,
    teilnehmer.so,
    teilnehmer.nurlog,
    teilnehmer.utc,
    teilnehmer.betreffalarm,
    teilnehmer.betreffentwarn,
        CASE
            WHEN ((teilnehmer.startdatum)::date > ('now'::text)::date) THEN false
            WHEN ((teilnehmer.stopdatum)::date < ('now'::text)::date) THEN false
            WHEN ((teilnehmer.startzeit)::time with time zone > ('now'::text)::time with time zone) THEN false
            WHEN ((teilnehmer.stopzeit)::time with time zone < ('now'::text)::time with time zone) THEN false
            WHEN (teilnehmer.nurlog = true) THEN false
            WHEN ((teilnehmer.mo = true) AND (date_part('dow'::text, now()) = (1)::double precision)) THEN true
            WHEN ((teilnehmer.di = true) AND (date_part('dow'::text, now()) = (2)::double precision)) THEN true
            WHEN ((teilnehmer.mi = true) AND (date_part('dow'::text, now()) = (3)::double precision)) THEN true
            WHEN ((teilnehmer.don = true) AND (date_part('dow'::text, now()) = (4)::double precision)) THEN true
            WHEN ((teilnehmer.fr = true) AND (date_part('dow'::text, now()) = (5)::double precision)) THEN true
            WHEN ((teilnehmer.sa = true) AND (date_part('dow'::text, now()) = (6)::double precision)) THEN true
            WHEN ((teilnehmer.so = true) AND (date_part('dow'::text, now()) = (0)::double precision)) THEN true
            ELSE false
        END AS aktiv
   FROM alarmserver.teilnehmer;


ALTER TABLE alarmserver.teilnehmer_aktiv OWNER TO postgres;

--
-- Name: VIEW teilnehmer_aktiv; Type: COMMENT; Schema: alarmserver; Owner: postgres
--

COMMENT ON VIEW alarmserver.teilnehmer_aktiv IS 'zeigt welcher alarm aktiv, also scharf geschaltet ist';


--
-- Name: teilnehmer_id_seq; Type: SEQUENCE; Schema: alarmserver; Owner: postgres
--

CREATE SEQUENCE alarmserver.teilnehmer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1333333
    CACHE 1
    CYCLE;


ALTER TABLE alarmserver.teilnehmer_id_seq OWNER TO postgres;

--
-- Name: teilnehmer_id_seq; Type: SEQUENCE OWNED BY; Schema: alarmserver; Owner: postgres
--

ALTER SEQUENCE alarmserver.teilnehmer_id_seq OWNED BY alarmserver.teilnehmer.id;


--
-- Name: adminlogins id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.adminlogins ALTER COLUMN id SET DEFAULT nextval('alarmserver.adminlogins_id_seq'::regclass);


--
-- Name: alarme id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.alarme ALTER COLUMN id SET DEFAULT nextval('alarmserver.alarme_id_seq'::regclass);


--
-- Name: gebiet2gruppe id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebiet2gruppe ALTER COLUMN id SET DEFAULT nextval('alarmserver.gebiet2gruppe_id_seq'::regclass);


--
-- Name: gebiete id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebiete ALTER COLUMN id SET DEFAULT nextval('alarmserver.gebiete_id_seq'::regclass);


--
-- Name: gebietgruppen id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebietgruppen ALTER COLUMN id SET DEFAULT nextval('alarmserver.gebietgruppen_id_seq'::regclass);


--
-- Name: kunden id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.kunden ALTER COLUMN id SET DEFAULT nextval('alarmserver.kunden_id_seq'::regclass);


--
-- Name: meldungen id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.meldungen ALTER COLUMN id SET DEFAULT nextval('alarmserver.meldungen_id_seq'::regclass);


--
-- Name: teilnehmer id; Type: DEFAULT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.teilnehmer ALTER COLUMN id SET DEFAULT nextval('alarmserver.teilnehmer_id_seq'::regclass);


--
-- Name: adminlogins adminlogins_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.adminlogins
    ADD CONSTRAINT adminlogins_pkey PRIMARY KEY (id);


--
-- Name: alarme alarm_gebiet_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.alarme
    ADD CONSTRAINT alarm_gebiet_uni UNIQUE (gebiet, teilnehmer);


--
-- Name: alarme alarm_gruppe_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.alarme
    ADD CONSTRAINT alarm_gruppe_uni UNIQUE (teilnehmer, gruppe);


--
-- Name: alarme alarme_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.alarme
    ADD CONSTRAINT alarme_pkey PRIMARY KEY (id);


--
-- Name: config config_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.config
    ADD CONSTRAINT config_pkey PRIMARY KEY (option);


--
-- Name: gebiet2gruppe geb2grp_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebiet2gruppe
    ADD CONSTRAINT geb2grp_pkey PRIMARY KEY (id);


--
-- Name: gebiet2gruppe geb2gruppe_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebiet2gruppe
    ADD CONSTRAINT geb2gruppe_uni UNIQUE (gruppe, gebiet);


--
-- Name: gebietgruppen gebgruppe_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebietgruppen
    ADD CONSTRAINT gebgruppe_pkey PRIMARY KEY (id);


--
-- Name: gebiete gebiete_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebiete
    ADD CONSTRAINT gebiete_pkey PRIMARY KEY (id);


--
-- Name: gebiete gebiete_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebiete
    ADD CONSTRAINT gebiete_uni UNIQUE (gebiet, radius, kunde);


--
-- Name: kunden kunden_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.kunden
    ADD CONSTRAINT kunden_pkey PRIMARY KEY (id);


--
-- Name: kundengruppen kundengruppen_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.kundengruppen
    ADD CONSTRAINT kundengruppen_pkey PRIMARY KEY (masterkunde, slavekunde);


--
-- Name: meldungen meldungen_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.meldungen
    ADD CONSTRAINT meldungen_pkey PRIMARY KEY (id);


--
-- Name: gebietgruppen name_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebietgruppen
    ADD CONSTRAINT name_uni UNIQUE (gruppenname, kunde);


--
-- Name: teilnehmer teilnehmer_pkey; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.teilnehmer
    ADD CONSTRAINT teilnehmer_pkey PRIMARY KEY (id);


--
-- Name: teilnehmer teilnehmer_uni; Type: CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.teilnehmer
    ADD CONSTRAINT teilnehmer_uni UNIQUE (teilnehmer, kunde);


--
-- Name: alarmgebiete_idx; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX alarmgebiete_idx ON alarmserver.gebiete USING gist (gebietgeom);


--
-- Name: fki_gebiete_fkey; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX fki_gebiete_fkey ON alarmserver.gebiet2gruppe USING btree (gebiet);


--
-- Name: fki_gruppe_fkey; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX fki_gruppe_fkey ON alarmserver.alarme USING btree (gruppe);


--
-- Name: fki_gruppen_fkey; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX fki_gruppen_fkey ON alarmserver.gebiet2gruppe USING btree (gruppe);


--
-- Name: idx_alarm; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX idx_alarm ON alarmserver.meldungen USING btree (alarm);


--
-- Name: idx_eintrag; Type: INDEX; Schema: alarmserver; Owner: postgres
--

CREATE INDEX idx_eintrag ON alarmserver.meldungen USING btree (eintrag);


--
-- Name: gebiete gebietalarm; Type: TRIGGER; Schema: alarmserver; Owner: postgres
--

CREATE TRIGGER gebietalarm BEFORE UPDATE ON alarmserver.gebiete FOR EACH ROW EXECUTE PROCEDURE alarmserver.gebietalarm();


--
-- Name: gebietgruppen gruppealarm; Type: TRIGGER; Schema: alarmserver; Owner: postgres
--

CREATE TRIGGER gruppealarm BEFORE UPDATE ON alarmserver.gebietgruppen FOR EACH ROW EXECUTE PROCEDURE alarmserver.gruppealarm();


--
-- Name: meldungen notify_on_alarm; Type: TRIGGER; Schema: alarmserver; Owner: postgres
--

CREATE TRIGGER notify_on_alarm BEFORE INSERT ON alarmserver.meldungen FOR EACH ROW EXECUTE PROCEDURE alarmserver.notify_alarm();


--
-- Name: gebiete update_surface_area; Type: TRIGGER; Schema: alarmserver; Owner: postgres
--

CREATE TRIGGER update_surface_area BEFORE INSERT OR UPDATE OF gebietgeom ON alarmserver.gebiete FOR EACH ROW EXECUTE PROCEDURE alarmserver.update_surface_area();


--
-- Name: alarme alarme_gebiet_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.alarme
    ADD CONSTRAINT alarme_gebiet_fkey FOREIGN KEY (gebiet) REFERENCES alarmserver.gebiete(id);


--
-- Name: alarme alarme_kunde_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.alarme
    ADD CONSTRAINT alarme_kunde_fkey FOREIGN KEY (kunde) REFERENCES alarmserver.kunden(id);


--
-- Name: alarme alarme_teilnehmer_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.alarme
    ADD CONSTRAINT alarme_teilnehmer_fkey FOREIGN KEY (teilnehmer) REFERENCES alarmserver.teilnehmer(id);


--
-- Name: gebiet2gruppe gebiet2gruppe_gebiet_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebiet2gruppe
    ADD CONSTRAINT gebiet2gruppe_gebiet_fkey FOREIGN KEY (gebiet) REFERENCES alarmserver.gebiete(id);


--
-- Name: gebietgruppen gebietgruppen_area_triggering_alarm_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebietgruppen
    ADD CONSTRAINT gebietgruppen_area_triggering_alarm_fkey FOREIGN KEY (area_triggering_alarm) REFERENCES alarmserver.gebiete(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gebiet2gruppe gruppen_fkey; Type: FK CONSTRAINT; Schema: alarmserver; Owner: postgres
--

ALTER TABLE ONLY alarmserver.gebiet2gruppe
    ADD CONSTRAINT gruppen_fkey FOREIGN KEY (gruppe) REFERENCES alarmserver.gebietgruppen(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA alarmserver; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA alarmserver TO current;
GRANT USAGE ON SCHEMA alarmserver TO alarmserver;
GRANT USAGE ON SCHEMA alarmserver TO kugelblitz;


--
-- Name: TABLE alarme; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE alarmserver.alarme TO geoserver;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alarmserver.alarme TO kugelblitz;


--
-- Name: SEQUENCE alarme_id_seq; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT USAGE ON SEQUENCE alarmserver.alarme_id_seq TO kugelblitz;


--
-- Name: TABLE kunden; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE alarmserver.kunden TO kugelblitz;


--
-- Name: TABLE config; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE alarmserver.config TO alarmserver;


--
-- Name: TABLE gebiet2gruppe; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE alarmserver.gebiet2gruppe TO kugelblitz;


--
-- Name: TABLE gebiete; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE alarmserver.gebiete TO current;
GRANT SELECT ON TABLE alarmserver.gebiete TO geoserver;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alarmserver.gebiete TO kugelblitz;


--
-- Name: SEQUENCE gebiete_id_seq; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT USAGE ON SEQUENCE alarmserver.gebiete_id_seq TO kugelblitz;


--
-- Name: TABLE gebietgruppen; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alarmserver.gebietgruppen TO kugelblitz;


--
-- Name: TABLE kundengruppen; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE alarmserver.kundengruppen TO geoserver;


--
-- Name: TABLE meldungen; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT,UPDATE ON TABLE alarmserver.meldungen TO alarmserver;
GRANT SELECT,INSERT ON TABLE alarmserver.meldungen TO kugelblitz;


--
-- Name: SEQUENCE meldungen_id_seq; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT USAGE ON SEQUENCE alarmserver.meldungen_id_seq TO kugelblitz;


--
-- Name: TABLE teilnehmer; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alarmserver.teilnehmer TO kugelblitz;


--
-- Name: TABLE teilnehmer_aktiv; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT SELECT ON TABLE alarmserver.teilnehmer_aktiv TO geoserver;
GRANT SELECT ON TABLE alarmserver.teilnehmer_aktiv TO kugelblitz;


--
-- Name: SEQUENCE teilnehmer_id_seq; Type: ACL; Schema: alarmserver; Owner: postgres
--

GRANT USAGE ON SEQUENCE alarmserver.teilnehmer_id_seq TO kugelblitz;


--
-- PostgreSQL database dump complete
--

-- Trigger: notify_on_stroke

-- DROP TRIGGER notify_on_stroke ON blitz.blitze2019;

CREATE TRIGGER notify_on_stroke
    BEFORE INSERT
    ON blitz.blitze2019
    FOR EACH ROW
    EXECUTE PROCEDURE alarmserver.notify_stroke();

-- Trigger: update_areas_on_stroke

-- DROP TRIGGER update_areas_on_stroke ON blitz.blitze2019;

CREATE TRIGGER update_areas_on_stroke
    AFTER INSERT
    ON blitz.blitze2019
    FOR EACH ROW
    EXECUTE PROCEDURE alarmserver.insert_stroke();
