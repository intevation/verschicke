--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-1.pgdg16.04+1)
-- Dumped by pg_dump version 11.2 (Ubuntu 11.2-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: blitz; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA blitz;


ALTER SCHEMA blitz OWNER TO postgres;

--
-- Name: blitz_userid(integer, integer, integer, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: blitz; Owner: postgres
--

CREATE FUNCTION blitz.blitz_userid(_userid integer, _zeit1 integer, _zeit2 integer, _co_left numeric, _co_right numeric, _co_top numeric, _co_bottom numeric) RETURNS SETOF public.liveblitz
    LANGUAGE plpgsql STABLE
    AS $_$

DECLARE
  _zeiten         record;
  _areaofinterest geometry;
BEGIN
  SELECT now() - make_interval(secs => _zeit1) AS startzeit,
         now() - make_interval(secs => _zeit2) AS stopzeit
    INTO _zeiten;

  SELECT st_intersection(
    st_transform(st_makeenvelope(_co_left, _co_top, _co_right, _co_bottom, 3857), 4326),
    g.blitzgebiet)
  FROM benutzer.benutzer b JOIN benutzer.gruppen g ON b.home_gruppen_id = g.id
  WHERE b.id = _userid
  INTO _areaofinterest;
  
  RETURN QUERY EXECUTE
    'SELECT id, zeit, typ::integer, strom, koordinate, tlp::integer,
           round(date_part(''EPOCH''::text, now() - zeit))::integer AS unixzeit
    FROM blitz.blitze
    WHERE zeit BETWEEN $1 AND $2
          AND koordinate && $3
    ORDER BY zeit;'
    USING _zeiten.startzeit, _zeiten.stopzeit, _areaofinterest;
  RETURN;
END;

$_$;


ALTER FUNCTION blitz.blitz_userid(_userid integer, _zeit1 integer, _zeit2 integer, _co_left numeric, _co_right numeric, _co_top numeric, _co_bottom numeric) OWNER TO postgres;

--
-- Name: ellipse(public.geometry, numeric, numeric, numeric, integer); Type: FUNCTION; Schema: blitz; Owner: postgres
--

CREATE FUNCTION blitz.ellipse(center public.geometry, major numeric, minor numeric, angle numeric, numvertices integer DEFAULT 8) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$DECLARE
 --centerWGS84 geometry := st_transform(center,3857);
 centerWGS84 geometry := st_transform(center,3035);
 rad numeric := radians(angle);
 incDeg numeric := 360.0/numVertices::numeric;
 x numeric;
 y numeric;
 ellipseLine geometry;
 ellipseArr geometry[];
 startpoint geometry;
 alpha numeric;
BEGIN
IF abs(major-round(major))+abs(minor-round(minor))>0 THEN
major := major * 1000;
minor := minor * 1000;
END IF;
FOR counter IN 0..numVertices LOOP
    alpha := radians(incDeg*counter);
	x := minor*sin(alpha);
	y := major*cos(alpha);
	IF counter = 0 THEN startpoint := st_translate(centerWGS84,(cos(rad)*x-sin(rad)*y)*1,(sin(rad)*x+cos(rad)*y)*1);
	END IF;
	ellipseArr := array_append(ellipseArr,st_translate(centerWGS84,(cos(rad)*x-sin(rad)*y)*1,(sin(rad)*x+cos(rad)*y)*1));
  END LOOP;
ellipseArr := array_append(ellipseArr,startpoint);													   
ellipseLine := st_makepolygon(st_transform(st_makeline(ellipseArr),4326));	
RETURN ellipseLine;
END; 

$$;


ALTER FUNCTION blitz.ellipse(center public.geometry, major numeric, minor numeric, angle numeric, numvertices integer) OWNER TO postgres;

--
-- Name: ellipse_userid(integer, integer, integer, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: blitz; Owner: postgres
--

CREATE FUNCTION blitz.ellipse_userid(_userid integer, _zeit1 integer, _zeit2 integer, _co_left numeric, _co_right numeric, _co_top numeric, _co_bottom numeric) RETURNS TABLE(id integer, zeit timestamp with time zone, sensorzahl numeric, tlp integer, strom numeric, unixzeit integer, major numeric, minor numeric, winkel numeric, typ integer, ellipse public.geometry)
    LANGUAGE plpgsql STABLE
    AS $_$

DECLARE
  _zeiten         record;
  _areaofinterest geometry;
BEGIN
  SELECT now() - make_interval(secs => _zeit1) AS startzeit,
         now() - make_interval(secs => _zeit2) AS stopzeit
    INTO _zeiten;

  SELECT st_intersection(
    st_transform(st_makeenvelope(_co_left, _co_top, _co_right, _co_bottom, 3857), 4326),
    g.blitzgebiet)
  FROM benutzer.benutzer b JOIN benutzer.gruppen g ON b.home_gruppen_id = g.id
  WHERE b.id = _userid
  INTO _areaofinterest;

RETURN QUERY EXECUTE
    'SELECT b.id, b.zeit, b.sensorzahl, b.tlp::integer, b.strom, round(date_part(''EPOCH''::text, now() - b.zeit))::integer AS unixzeit,
			blitz.unify(b.grosseachse) as grosseachse,blitz.unify(b.kleineachse) as kleineachse, b.winkelachse,b.typ::integer,
			blitz.ellipse(koordinate,grosseachse, kleineachse, winkelachse, 32)														  
    FROM blitz.blitze b
    WHERE b.zeit BETWEEN $1 AND $2
          AND b.koordinate && $3
    ORDER BY b.zeit;'
	USING _zeiten.startzeit, _zeiten.stopzeit, _areaofinterest;
	
  RETURN;
END;

$_$;


ALTER FUNCTION blitz.ellipse_userid(_userid integer, _zeit1 integer, _zeit2 integer, _co_left numeric, _co_right numeric, _co_top numeric, _co_bottom numeric) OWNER TO postgres;

--
-- Name: sync_blitze_to_testdb(); Type: FUNCTION; Schema: blitz; Owner: postgres
--

CREATE FUNCTION blitz.sync_blitze_to_testdb() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
BEGIN
  PERFORM dblink_exec('dbname=blids2_intevation',
              format($$INSERT INTO blitz.blitze2018 (zeit,
                                                typ,
                                                strom, 
                                                eintrag,
                                                koordinate,
                                                grosseachse,
                                                kleineachse,
                                                winkelachse,
                                                freigrade,
                                                chiquadrat,
                                                steigzeit,
                                                spitzenull,
                                                sensorzahl,
                                                hoehe,
                                                tlp)
                             VALUES ('%s',%s,%s,'%s','%s',%s,%s,%s,
                                     %s,%s,%s,%s,%s,%s,%s);$$,
                     NEW.zeit,
                     NEW.typ,
                     NEW.strom, 
                     NEW.eintrag,
                     NEW.koordinate,
                     NEW.grosseachse,
                     NEW.kleineachse,
                     NEW.winkelachse,
                     NEW.freigrade,
                     NEW.chiquadrat,
                     NEW.steigzeit,
                     NEW.spitzenull,
                     NEW.sensorzahl,
                     NEW.hoehe,
                     NEW.tlp),
              false);
  RETURN NEW;
END;
$_$;


ALTER FUNCTION blitz.sync_blitze_to_testdb() OWNER TO postgres;

--
-- Name: unify(numeric); Type: FUNCTION; Schema: blitz; Owner: postgres
--

CREATE FUNCTION blitz.unify(value numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
BEGIN
IF abs(value-round(value))>0 THEN
value := value * 1000;
END IF;
RETURN value;
END; 

$$;


ALTER FUNCTION blitz.unify(value numeric) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: blitze; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze (
    id integer NOT NULL,
    zeit timestamp with time zone,
    typ smallint,
    strom numeric,
    eintrag timestamp with time zone DEFAULT now(),
    koordinate public.geometry(Point,4326),
    grosseachse numeric,
    kleineachse numeric,
    winkelachse numeric,
    freigrade integer,
    chiquadrat numeric,
    steigzeit numeric,
    spitzenull numeric,
    sensorzahl numeric,
    hoehe numeric DEFAULT 0,
    tlp numeric DEFAULT 1,
    maxraterise numeric,
    hoeheungenau numeric,
    strokezahl integer,
    pulsezahl integer
);


ALTER TABLE blitz.blitze OWNER TO postgres;

--
-- Name: blitze1992; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze1992 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1992-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '1993-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze1992 OWNER TO postgres;

--
-- Name: blitze1993; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze1993 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1993-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '1994-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze1993 OWNER TO postgres;

--
-- Name: blitze1994; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze1994 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1994-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '1995-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze1994 OWNER TO postgres;

--
-- Name: blitze1995; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze1995 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1995-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '1996-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze1995 OWNER TO postgres;

--
-- Name: blitze1996; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze1996 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1996-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '1997-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze1996 OWNER TO postgres;

--
-- Name: blitze1997; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze1997 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1997-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '1998-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze1997 OWNER TO postgres;

--
-- Name: blitze1998; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze1998 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1998-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '1999-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze1998 OWNER TO postgres;

--
-- Name: blitze1999; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze1999 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '1999-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2000-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze1999 OWNER TO postgres;

--
-- Name: blitze2000; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2000 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2000-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2001-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2000 OWNER TO postgres;

--
-- Name: blitze2001; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2001 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2001-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2002-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2001 OWNER TO postgres;

--
-- Name: blitze2002; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2002 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2002-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2003-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2002 OWNER TO postgres;

--
-- Name: blitze2003; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2003 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2003-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2004-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2003 OWNER TO postgres;

--
-- Name: blitze2004; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2004 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2004-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2005-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2004 OWNER TO postgres;

--
-- Name: blitze2005; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2005 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2005-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2006-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2005 OWNER TO postgres;

--
-- Name: blitze2006; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2006 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2006-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2007-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2006 OWNER TO postgres;

--
-- Name: blitze2007; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2007 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2007-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2008-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2007 OWNER TO postgres;

--
-- Name: blitze2008; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2008 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2008-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2009-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2008 OWNER TO postgres;

--
-- Name: blitze2009; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2009 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2009-01-01 00:00:00.01+01'::timestamp with time zone) AND (zeit <= '2010-01-01 00:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2009 OWNER TO postgres;

--
-- Name: blitze2010; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2010 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2010-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2011-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2010 OWNER TO postgres;

--
-- Name: blitze2011; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2011 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2011-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2012-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2011 OWNER TO postgres;

--
-- Name: blitze2012; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2012 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2012-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2013-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2012 OWNER TO postgres;

--
-- Name: blitze2013; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2013 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2013-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2014-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2013 OWNER TO postgres;

--
-- Name: blitze2014; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2014 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2014-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2015-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2014 OWNER TO postgres;

--
-- Name: blitze2015; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2015 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2015-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2016-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2015 OWNER TO postgres;

--
-- Name: blitze2016; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2016 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2016-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2017-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2016 OWNER TO postgres;

--
-- Name: blitze2017; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2017 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2017-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2018-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2017 OWNER TO postgres;

--
-- Name: blitze2018; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2018 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2018-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2019-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2018 OWNER TO postgres;

--
-- Name: blitze2019; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.blitze2019 (
    CONSTRAINT blitze_zeit CHECK (((zeit >= '2019-01-01 00:00:00+01'::timestamp with time zone) AND (zeit <= '2020-01-01 00:00:00+01'::timestamp with time zone)))
)
INHERITS (blitz.blitze);


ALTER TABLE blitz.blitze2019 OWNER TO postgres;

--
-- Name: blitze_id_seq; Type: SEQUENCE; Schema: blitz; Owner: postgres
--

CREATE SEQUENCE blitz.blitze_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blitz.blitze_id_seq OWNER TO postgres;

--
-- Name: blitze_id_seq; Type: SEQUENCE OWNED BY; Schema: blitz; Owner: postgres
--

ALTER SEQUENCE blitz.blitze_id_seq OWNED BY blitz.blitze.id;


--
-- Name: flashes; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes (
    id integer NOT NULL,
    zeit timestamp with time zone,
    typ smallint,
    strom numeric,
    koordinate public.geometry(Point,4326),
    grosseachse numeric,
    achsenverhaeltnis numeric,
    winkelachse numeric,
    strokezahl integer
);


ALTER TABLE blitz.flashes OWNER TO postgres;

--
-- Name: flashes2005; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2005 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2005-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2006-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2005 OWNER TO postgres;

--
-- Name: flashes2006; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2006 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2006-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2007-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2006 OWNER TO postgres;

--
-- Name: flashes2007; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2007 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2007-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2008-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2007 OWNER TO postgres;

--
-- Name: flashes2008; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2008 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2008-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2009-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2008 OWNER TO postgres;

--
-- Name: flashes2009; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2009 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2009-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2010-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2009 OWNER TO postgres;

--
-- Name: flashes2010; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2010 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2010-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2011-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2010 OWNER TO postgres;

--
-- Name: flashes2011; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2011 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2011-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2012-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2011 OWNER TO postgres;

--
-- Name: flashes2012; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2012 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2012-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2013-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2012 OWNER TO postgres;

--
-- Name: flashes2013; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2013 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2013-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2014-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2013 OWNER TO postgres;

--
-- Name: flashes2014; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2014 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2014-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2015-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2014 OWNER TO postgres;

--
-- Name: flashes2015; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2015 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2015-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2016-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2015 OWNER TO postgres;

--
-- Name: flashes2016; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2016 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2016-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2017-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2016 OWNER TO postgres;

--
-- Name: flashes2017; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2017 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2017-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2018-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2017 OWNER TO postgres;

--
-- Name: flashes2018; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.flashes2018 (
    CONSTRAINT flashes_zeit CHECK (((zeit >= '2018-01-01 01:00:00.01+01'::timestamp with time zone) AND (zeit <= '2019-01-01 01:00:00.01+01'::timestamp with time zone)))
)
INHERITS (blitz.flashes);


ALTER TABLE blitz.flashes2018 OWNER TO postgres;

--
-- Name: flashes_id_seq; Type: SEQUENCE; Schema: blitz; Owner: postgres
--

CREATE SEQUENCE blitz.flashes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blitz.flashes_id_seq OWNER TO postgres;

--
-- Name: flashes_id_seq; Type: SEQUENCE OWNED BY; Schema: blitz; Owner: postgres
--

ALTER SEQUENCE blitz.flashes_id_seq OWNED BY blitz.flashes.id;


--
-- Name: vergleich; Type: TABLE; Schema: blitz; Owner: postgres
--

CREATE TABLE blitz.vergleich (
    id integer NOT NULL,
    datum date NOT NULL,
    db_de integer,
    cats_de integer,
    db_eu integer,
    cats_eu integer,
    freigabe boolean DEFAULT false NOT NULL,
    eintrag timestamp with time zone DEFAULT now()
);


ALTER TABLE blitz.vergleich OWNER TO postgres;

--
-- Name: vergleich_id_seq; Type: SEQUENCE; Schema: blitz; Owner: postgres
--

CREATE SEQUENCE blitz.vergleich_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blitz.vergleich_id_seq OWNER TO postgres;

--
-- Name: vergleich_id_seq; Type: SEQUENCE OWNED BY; Schema: blitz; Owner: postgres
--

ALTER SEQUENCE blitz.vergleich_id_seq OWNED BY blitz.vergleich.id;


--
-- Name: blitze id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1992 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1992 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1992 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1992 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1992 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1992 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1992 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1992 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1993 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1993 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1993 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1993 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1993 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1993 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1993 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1993 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1994 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1994 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1994 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1994 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1994 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1994 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1994 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1994 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1995 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1995 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1995 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1995 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1995 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1995 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1995 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1995 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1996 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1996 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1996 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1996 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1996 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1996 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1996 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1996 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1997 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1997 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1997 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1997 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1997 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1997 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1997 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1997 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1998 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1998 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1998 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1998 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1998 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1998 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1998 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1998 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze1999 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1999 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze1999 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1999 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze1999 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1999 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze1999 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1999 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2000 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2000 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2000 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2000 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2000 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2000 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2000 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2000 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2001 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2001 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2001 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2001 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2001 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2001 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2001 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2001 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2002 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2002 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2002 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2002 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2002 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2002 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2002 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2002 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2003 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2003 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2003 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2003 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2003 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2003 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2003 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2003 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2004 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2004 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2004 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2004 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2004 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2004 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2004 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2004 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2005 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2005 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2005 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2005 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2005 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2005 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2005 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2005 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2006 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2006 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2006 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2006 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2006 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2006 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2006 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2006 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2007 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2007 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2007 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2007 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2007 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2007 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2007 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2007 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2008 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2008 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2008 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2008 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2008 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2008 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2008 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2008 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2009 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2009 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2009 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2009 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2009 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2009 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2009 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2009 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2010 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2010 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2010 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2010 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2010 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2010 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2010 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2010 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2011 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2011 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2011 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2011 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2011 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2011 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2011 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2011 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2012 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2012 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2012 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2012 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2012 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2012 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2012 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2012 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2013 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2013 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2013 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2013 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2013 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2013 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2013 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2013 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2014 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2014 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2014 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2014 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2014 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2014 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2014 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2014 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2015 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2015 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2015 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2015 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2015 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2015 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2015 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2015 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2016 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2016 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2016 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2016 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2016 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2016 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2016 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2016 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2017 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2017 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2017 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2017 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2017 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2017 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2017 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2017 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2018 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2018 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2018 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2018 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2018 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2018 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2018 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2018 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: blitze2019 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2019 ALTER COLUMN id SET DEFAULT nextval('blitz.blitze_id_seq'::regclass);


--
-- Name: blitze2019 eintrag; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2019 ALTER COLUMN eintrag SET DEFAULT now();


--
-- Name: blitze2019 hoehe; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2019 ALTER COLUMN hoehe SET DEFAULT 0;


--
-- Name: blitze2019 tlp; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2019 ALTER COLUMN tlp SET DEFAULT 1;


--
-- Name: flashes id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2005 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2005 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2006 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2006 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2007 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2007 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2008 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2008 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2009 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2009 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2010 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2010 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2011 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2011 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2012 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2012 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2013 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2013 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2014 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2014 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2015 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2015 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2016 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2016 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2017 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2017 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: flashes2018 id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2018 ALTER COLUMN id SET DEFAULT nextval('blitz.flashes_id_seq'::regclass);


--
-- Name: vergleich id; Type: DEFAULT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.vergleich ALTER COLUMN id SET DEFAULT nextval('blitz.vergleich_id_seq'::regclass);


--
-- Name: vergleich blitzdaten_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.vergleich
    ADD CONSTRAINT blitzdaten_pkey PRIMARY KEY (id);


--
-- Name: blitze1992 blitze1992_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1992
    ADD CONSTRAINT blitze1992_pkey PRIMARY KEY (id);


--
-- Name: blitze1992 blitze1992_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1992
    ADD CONSTRAINT blitze1992_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1993 blitze1993_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1993
    ADD CONSTRAINT blitze1993_pkey PRIMARY KEY (id);


--
-- Name: blitze1993 blitze1993_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1993
    ADD CONSTRAINT blitze1993_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1994 blitze1994_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1994
    ADD CONSTRAINT blitze1994_pkey PRIMARY KEY (id);


--
-- Name: blitze1994 blitze1994_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1994
    ADD CONSTRAINT blitze1994_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1995 blitze1995_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1995
    ADD CONSTRAINT blitze1995_pkey PRIMARY KEY (id);


--
-- Name: blitze1995 blitze1995_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1995
    ADD CONSTRAINT blitze1995_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1996 blitze1996_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1996
    ADD CONSTRAINT blitze1996_pkey PRIMARY KEY (id);


--
-- Name: blitze1996 blitze1996_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1996
    ADD CONSTRAINT blitze1996_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1997 blitze1997_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1997
    ADD CONSTRAINT blitze1997_pkey PRIMARY KEY (id);


--
-- Name: blitze1997 blitze1997_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1997
    ADD CONSTRAINT blitze1997_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1998 blitze1998_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1998
    ADD CONSTRAINT blitze1998_pkey PRIMARY KEY (id);


--
-- Name: blitze1998 blitze1998_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1998
    ADD CONSTRAINT blitze1998_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze1999 blitze1999_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1999
    ADD CONSTRAINT blitze1999_pkey PRIMARY KEY (id);


--
-- Name: blitze1999 blitze1999_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze1999
    ADD CONSTRAINT blitze1999_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2000 blitze2000_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2000
    ADD CONSTRAINT blitze2000_pkey PRIMARY KEY (id);


--
-- Name: blitze2000 blitze2000_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2000
    ADD CONSTRAINT blitze2000_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2001 blitze2001_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2001
    ADD CONSTRAINT blitze2001_pkey PRIMARY KEY (id);


--
-- Name: blitze2001 blitze2001_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2001
    ADD CONSTRAINT blitze2001_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2002 blitze2002_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2002
    ADD CONSTRAINT blitze2002_pkey PRIMARY KEY (id);


--
-- Name: blitze2002 blitze2002_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2002
    ADD CONSTRAINT blitze2002_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2003 blitze2003_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2003
    ADD CONSTRAINT blitze2003_pkey PRIMARY KEY (id);


--
-- Name: blitze2003 blitze2003_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2003
    ADD CONSTRAINT blitze2003_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2004 blitze2004_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2004
    ADD CONSTRAINT blitze2004_pkey PRIMARY KEY (id);


--
-- Name: blitze2004 blitze2004_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2004
    ADD CONSTRAINT blitze2004_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2005 blitze2005_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2005
    ADD CONSTRAINT blitze2005_pkey PRIMARY KEY (id);


--
-- Name: blitze2005 blitze2005_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2005
    ADD CONSTRAINT blitze2005_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2006 blitze2006_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2006
    ADD CONSTRAINT blitze2006_pkey PRIMARY KEY (id);


--
-- Name: blitze2006 blitze2006_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2006
    ADD CONSTRAINT blitze2006_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2007 blitze2007_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2007
    ADD CONSTRAINT blitze2007_pkey PRIMARY KEY (id);


--
-- Name: blitze2007 blitze2007_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2007
    ADD CONSTRAINT blitze2007_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2008 blitze2008_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2008
    ADD CONSTRAINT blitze2008_pkey PRIMARY KEY (id);


--
-- Name: blitze2008 blitze2008_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2008
    ADD CONSTRAINT blitze2008_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2009 blitze2009_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2009
    ADD CONSTRAINT blitze2009_pkey PRIMARY KEY (id);


--
-- Name: blitze2009 blitze2009_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2009
    ADD CONSTRAINT blitze2009_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: blitze2010 blitze2010_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2010
    ADD CONSTRAINT blitze2010_pkey PRIMARY KEY (id);


--
-- Name: blitze2011 blitze2011_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2011
    ADD CONSTRAINT blitze2011_pkey PRIMARY KEY (id);


--
-- Name: blitze2012 blitze2012_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2012
    ADD CONSTRAINT blitze2012_pkey PRIMARY KEY (id);


--
-- Name: blitze2013 blitze2013_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2013
    ADD CONSTRAINT blitze2013_pkey PRIMARY KEY (id);


--
-- Name: blitze2014 blitze2014_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2014
    ADD CONSTRAINT blitze2014_pkey PRIMARY KEY (id);


--
-- Name: blitze2015 blitze2015_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2015
    ADD CONSTRAINT blitze2015_pkey PRIMARY KEY (id);


--
-- Name: blitze2016 blitze2016_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2016
    ADD CONSTRAINT blitze2016_pkey PRIMARY KEY (id);


--
-- Name: blitze2017 blitze2017_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2017
    ADD CONSTRAINT blitze2017_pkey PRIMARY KEY (id);


--
-- Name: blitze2018 blitze2018_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2018
    ADD CONSTRAINT blitze2018_pkey PRIMARY KEY (id);


--
-- Name: blitze2019 blitze2019_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze2019
    ADD CONSTRAINT blitze2019_pkey PRIMARY KEY (id);


--
-- Name: blitze blitze_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze
    ADD CONSTRAINT blitze_pkey PRIMARY KEY (id);


--
-- Name: blitze blitze_zeit_typ_strom_key; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.blitze
    ADD CONSTRAINT blitze_zeit_typ_strom_key UNIQUE (zeit, typ, strom);


--
-- Name: flashes2005 flashes2005_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2005
    ADD CONSTRAINT flashes2005_pkey PRIMARY KEY (id);


--
-- Name: flashes2006 flashes2006_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2006
    ADD CONSTRAINT flashes2006_pkey PRIMARY KEY (id);


--
-- Name: flashes2007 flashes2007_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2007
    ADD CONSTRAINT flashes2007_pkey PRIMARY KEY (id);


--
-- Name: flashes2008 flashes2008_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2008
    ADD CONSTRAINT flashes2008_pkey PRIMARY KEY (id);


--
-- Name: flashes2009 flashes2009_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2009
    ADD CONSTRAINT flashes2009_pkey PRIMARY KEY (id);


--
-- Name: flashes2010 flashes2010_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2010
    ADD CONSTRAINT flashes2010_pkey PRIMARY KEY (id);


--
-- Name: flashes2011 flashes2011_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2011
    ADD CONSTRAINT flashes2011_pkey PRIMARY KEY (id);


--
-- Name: flashes2012 flashes2012_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2012
    ADD CONSTRAINT flashes2012_pkey PRIMARY KEY (id);


--
-- Name: flashes2013 flashes2013_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2013
    ADD CONSTRAINT flashes2013_pkey PRIMARY KEY (id);


--
-- Name: flashes2014 flashes2014_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2014
    ADD CONSTRAINT flashes2014_pkey PRIMARY KEY (id);


--
-- Name: flashes2015 flashes2015_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2015
    ADD CONSTRAINT flashes2015_pkey PRIMARY KEY (id);


--
-- Name: flashes2016 flashes2016_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2016
    ADD CONSTRAINT flashes2016_pkey PRIMARY KEY (id);


--
-- Name: flashes2017 flashes2017_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2017
    ADD CONSTRAINT flashes2017_pkey PRIMARY KEY (id);


--
-- Name: flashes2018 flashes2018_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes2018
    ADD CONSTRAINT flashes2018_pkey PRIMARY KEY (id);


--
-- Name: flashes flashes_pkey; Type: CONSTRAINT; Schema: blitz; Owner: postgres
--

ALTER TABLE ONLY blitz.flashes
    ADD CONSTRAINT flashes_pkey PRIMARY KEY (id);


--
-- Name: b_koord_1992_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1992_idx ON blitz.blitze1992 USING gist (koordinate);


--
-- Name: b_koord_1993_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1993_idx ON blitz.blitze1993 USING gist (koordinate);


--
-- Name: b_koord_1994_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1994_idx ON blitz.blitze1994 USING gist (koordinate);


--
-- Name: b_koord_1995_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1995_idx ON blitz.blitze1995 USING gist (koordinate);


--
-- Name: b_koord_1996_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1996_idx ON blitz.blitze1996 USING gist (koordinate);


--
-- Name: b_koord_1997_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1997_idx ON blitz.blitze1997 USING gist (koordinate);


--
-- Name: b_koord_1998_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1998_idx ON blitz.blitze1998 USING gist (koordinate);


--
-- Name: b_koord_1999_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_1999_idx ON blitz.blitze1999 USING gist (koordinate);


--
-- Name: b_koord_2000_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2000_idx ON blitz.blitze2000 USING gist (koordinate);


--
-- Name: b_koord_2001_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2001_idx ON blitz.blitze2001 USING gist (koordinate);


--
-- Name: b_koord_2002_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2002_idx ON blitz.blitze2002 USING gist (koordinate);


--
-- Name: b_koord_2003_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2003_idx ON blitz.blitze2003 USING gist (koordinate);


--
-- Name: b_koord_2004_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2004_idx ON blitz.blitze2004 USING gist (koordinate);


--
-- Name: b_koord_2005_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2005_idx ON blitz.blitze2005 USING gist (koordinate);


--
-- Name: b_koord_2006_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2006_idx ON blitz.blitze2006 USING gist (koordinate);


--
-- Name: b_koord_2007_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2007_idx ON blitz.blitze2007 USING gist (koordinate);


--
-- Name: b_koord_2008_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2008_idx ON blitz.blitze2008 USING gist (koordinate);


--
-- Name: b_koord_2009_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2009_idx ON blitz.blitze2009 USING gist (koordinate);


--
-- Name: b_koord_2010_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2010_idx ON blitz.blitze2010 USING gist (koordinate);


--
-- Name: b_koord_2011_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2011_idx ON blitz.blitze2011 USING gist (koordinate);


--
-- Name: b_koord_2012_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2012_idx ON blitz.blitze2012 USING gist (koordinate);


--
-- Name: b_koord_2013_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2013_idx ON blitz.blitze2013 USING gist (koordinate);


--
-- Name: b_koord_2014_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2014_idx ON blitz.blitze2014 USING gist (koordinate);


--
-- Name: b_koord_2015_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2015_idx ON blitz.blitze2015 USING gist (koordinate);


--
-- Name: b_koord_2016_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2016_idx ON blitz.blitze2016 USING gist (koordinate);


--
-- Name: b_koord_2017_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2017_idx ON blitz.blitze2017 USING gist (koordinate);


--
-- Name: b_koord_2018_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2018_idx ON blitz.blitze2018 USING gist (koordinate);


--
-- Name: b_koord_2019_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_koord_2019_idx ON blitz.blitze2019 USING gist (koordinate);


--
-- Name: b_zeit_1992_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1992_idx ON blitz.blitze1992 USING btree (zeit);


--
-- Name: b_zeit_1993_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1993_idx ON blitz.blitze1993 USING btree (zeit);


--
-- Name: b_zeit_1994_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1994_idx ON blitz.blitze1994 USING btree (zeit);


--
-- Name: b_zeit_1995_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1995_idx ON blitz.blitze1995 USING btree (zeit);


--
-- Name: b_zeit_1996_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1996_idx ON blitz.blitze1996 USING btree (zeit);


--
-- Name: b_zeit_1997_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1997_idx ON blitz.blitze1997 USING btree (zeit);


--
-- Name: b_zeit_1998_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1998_idx ON blitz.blitze1998 USING btree (zeit);


--
-- Name: b_zeit_1999_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_1999_idx ON blitz.blitze1999 USING btree (zeit);


--
-- Name: b_zeit_2000_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2000_idx ON blitz.blitze2000 USING btree (zeit);


--
-- Name: b_zeit_2001_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2001_idx ON blitz.blitze2001 USING btree (zeit);


--
-- Name: b_zeit_2002_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2002_idx ON blitz.blitze2002 USING btree (zeit);


--
-- Name: b_zeit_2003_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2003_idx ON blitz.blitze2003 USING btree (zeit);


--
-- Name: b_zeit_2004_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2004_idx ON blitz.blitze2004 USING btree (zeit);


--
-- Name: b_zeit_2005_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2005_idx ON blitz.blitze2005 USING btree (zeit);


--
-- Name: b_zeit_2006_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2006_idx ON blitz.blitze2006 USING btree (zeit);


--
-- Name: b_zeit_2007_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2007_idx ON blitz.blitze2007 USING btree (zeit);


--
-- Name: b_zeit_2008_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2008_idx ON blitz.blitze2008 USING btree (zeit);


--
-- Name: b_zeit_2009_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2009_idx ON blitz.blitze2009 USING btree (zeit);


--
-- Name: b_zeit_2010_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2010_idx ON blitz.blitze2010 USING btree (zeit);


--
-- Name: b_zeit_2011_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2011_idx ON blitz.blitze2011 USING btree (zeit);


--
-- Name: b_zeit_2012_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2012_idx ON blitz.blitze2012 USING btree (zeit);


--
-- Name: b_zeit_2013_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2013_idx ON blitz.blitze2013 USING btree (zeit);


--
-- Name: b_zeit_2014_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2014_idx ON blitz.blitze2014 USING btree (zeit);


--
-- Name: b_zeit_2015_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2015_idx ON blitz.blitze2015 USING btree (zeit);


--
-- Name: b_zeit_2016_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2016_idx ON blitz.blitze2016 USING btree (zeit);


--
-- Name: b_zeit_2017_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2017_idx ON blitz.blitze2017 USING btree (zeit);


--
-- Name: b_zeit_2018_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2018_idx ON blitz.blitze2018 USING btree (zeit);


--
-- Name: b_zeit_2019_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX b_zeit_2019_idx ON blitz.blitze2019 USING btree (zeit);


--
-- Name: f_koord_2005_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2005_idx ON blitz.flashes2005 USING gist (koordinate);


--
-- Name: f_koord_2006_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2006_idx ON blitz.flashes2006 USING gist (koordinate);


--
-- Name: f_koord_2007_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2007_idx ON blitz.flashes2007 USING gist (koordinate);


--
-- Name: f_koord_2008_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2008_idx ON blitz.flashes2008 USING gist (koordinate);


--
-- Name: f_koord_2009_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2009_idx ON blitz.flashes2009 USING gist (koordinate);


--
-- Name: f_koord_2010_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2010_idx ON blitz.flashes2010 USING gist (koordinate);


--
-- Name: f_koord_2011_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2011_idx ON blitz.flashes2011 USING gist (koordinate);


--
-- Name: f_koord_2012_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2012_idx ON blitz.flashes2012 USING gist (koordinate);


--
-- Name: f_koord_2013_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2013_idx ON blitz.flashes2013 USING gist (koordinate);


--
-- Name: f_koord_2014_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2014_idx ON blitz.flashes2014 USING gist (koordinate);


--
-- Name: f_koord_2015_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2015_idx ON blitz.flashes2015 USING gist (koordinate);


--
-- Name: f_koord_2016_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2016_idx ON blitz.flashes2016 USING gist (koordinate);


--
-- Name: f_koord_2017_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2017_idx ON blitz.flashes2017 USING gist (koordinate);


--
-- Name: f_koord_2018_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_koord_2018_idx ON blitz.flashes2018 USING gist (koordinate);


--
-- Name: f_zeit_2005_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2005_idx ON blitz.flashes2005 USING btree (zeit);


--
-- Name: f_zeit_2006_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2006_idx ON blitz.flashes2006 USING btree (zeit);


--
-- Name: f_zeit_2007_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2007_idx ON blitz.flashes2007 USING btree (zeit);


--
-- Name: f_zeit_2008_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2008_idx ON blitz.flashes2008 USING btree (zeit);


--
-- Name: f_zeit_2009_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2009_idx ON blitz.flashes2009 USING btree (zeit);


--
-- Name: f_zeit_2010_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2010_idx ON blitz.flashes2010 USING btree (zeit);


--
-- Name: f_zeit_2011_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2011_idx ON blitz.flashes2011 USING btree (zeit);


--
-- Name: f_zeit_2012_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2012_idx ON blitz.flashes2012 USING btree (zeit);


--
-- Name: f_zeit_2013_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2013_idx ON blitz.flashes2013 USING btree (zeit);


--
-- Name: f_zeit_2014_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2014_idx ON blitz.flashes2014 USING btree (zeit);


--
-- Name: f_zeit_2015_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2015_idx ON blitz.flashes2015 USING btree (zeit);


--
-- Name: f_zeit_2016_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2016_idx ON blitz.flashes2016 USING btree (zeit);


--
-- Name: f_zeit_2017_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2017_idx ON blitz.flashes2017 USING btree (zeit);


--
-- Name: f_zeit_2018_idx; Type: INDEX; Schema: blitz; Owner: postgres
--

CREATE INDEX f_zeit_2018_idx ON blitz.flashes2018 USING btree (zeit);


--
-- Name: blitze1992 blids1992_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1992_del_protect AS
    ON DELETE TO blitz.blitze1992 DO INSTEAD NOTHING;


--
-- Name: blitze1993 blids1993_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1993_del_protect AS
    ON DELETE TO blitz.blitze1993 DO INSTEAD NOTHING;


--
-- Name: blitze1994 blids1994_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1994_del_protect AS
    ON DELETE TO blitz.blitze1994 DO INSTEAD NOTHING;


--
-- Name: blitze1995 blids1995_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1995_del_protect AS
    ON DELETE TO blitz.blitze1995 DO INSTEAD NOTHING;


--
-- Name: blitze1996 blids1996_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1996_del_protect AS
    ON DELETE TO blitz.blitze1996 DO INSTEAD NOTHING;


--
-- Name: blitze1997 blids1997_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1997_del_protect AS
    ON DELETE TO blitz.blitze1997 DO INSTEAD NOTHING;


--
-- Name: blitze1998 blids1998_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1998_del_protect AS
    ON DELETE TO blitz.blitze1998 DO INSTEAD NOTHING;


--
-- Name: blitze1999 blids1999_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids1999_del_protect AS
    ON DELETE TO blitz.blitze1999 DO INSTEAD NOTHING;


--
-- Name: blitze2000 blids2000_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2000_del_protect AS
    ON DELETE TO blitz.blitze2000 DO INSTEAD NOTHING;


--
-- Name: blitze2001 blids2001_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2001_del_protect AS
    ON DELETE TO blitz.blitze2001 DO INSTEAD NOTHING;


--
-- Name: blitze2002 blids2002_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2002_del_protect AS
    ON DELETE TO blitz.blitze2002 DO INSTEAD NOTHING;


--
-- Name: blitze2003 blids2003_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2003_del_protect AS
    ON DELETE TO blitz.blitze2003 DO INSTEAD NOTHING;


--
-- Name: blitze2004 blids2004_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2004_del_protect AS
    ON DELETE TO blitz.blitze2004 DO INSTEAD NOTHING;


--
-- Name: blitze2005 blids2005_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2005_del_protect AS
    ON DELETE TO blitz.blitze2005 DO INSTEAD NOTHING;


--
-- Name: blitze2006 blids2006_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2006_del_protect AS
    ON DELETE TO blitz.blitze2006 DO INSTEAD NOTHING;


--
-- Name: blitze2007 blids2007_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2007_del_protect AS
    ON DELETE TO blitz.blitze2007 DO INSTEAD NOTHING;


--
-- Name: blitze2008 blids2008_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2008_del_protect AS
    ON DELETE TO blitz.blitze2008 DO INSTEAD NOTHING;


--
-- Name: blitze2009 blids2009_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2009_del_protect AS
    ON DELETE TO blitz.blitze2009 DO INSTEAD NOTHING;


--
-- Name: blitze2010 blids2010_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2010_del_protect AS
    ON DELETE TO blitz.blitze2010 DO INSTEAD NOTHING;


--
-- Name: blitze2011 blids2011_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2011_del_protect AS
    ON DELETE TO blitz.blitze2011 DO INSTEAD NOTHING;


--
-- Name: blitze2012 blids2012_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2012_del_protect AS
    ON DELETE TO blitz.blitze2012 DO INSTEAD NOTHING;


--
-- Name: blitze2013 blids2013_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2013_del_protect AS
    ON DELETE TO blitz.blitze2013 DO INSTEAD NOTHING;


--
-- Name: blitze2014 blids2014_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2014_del_protect AS
    ON DELETE TO blitz.blitze2014 DO INSTEAD NOTHING;


--
-- Name: blitze2015 blids2015_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2015_del_protect AS
    ON DELETE TO blitz.blitze2015 DO INSTEAD NOTHING;


--
-- Name: blitze2016 blids2016_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2016_del_protect AS
    ON DELETE TO blitz.blitze2016 DO INSTEAD NOTHING;


--
-- Name: blitze2018 blids2018_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2018_del_protect AS
    ON DELETE TO blitz.blitze2018 DO INSTEAD NOTHING;


--
-- Name: blitze2019 blids2019_del_protect; Type: RULE; Schema: blitz; Owner: postgres
--

CREATE RULE blids2019_del_protect AS
    ON DELETE TO blitz.blitze2019 DO INSTEAD NOTHING;



--
-- Name: SCHEMA blitz; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA blitz TO blitze;
GRANT USAGE ON SCHEMA blitz TO geoserver;


--
-- Name: FUNCTION blitz_userid(_userid integer, _zeit1 integer, _zeit2 integer, _co_left numeric, _co_right numeric, _co_top numeric, _co_bottom numeric); Type: ACL; Schema: blitz; Owner: postgres
--

GRANT ALL ON FUNCTION blitz.blitz_userid(_userid integer, _zeit1 integer, _zeit2 integer, _co_left numeric, _co_right numeric, _co_top numeric, _co_bottom numeric) TO kugelblitz;


--
-- Name: FUNCTION ellipse(center public.geometry, major numeric, minor numeric, angle numeric, numvertices integer); Type: ACL; Schema: blitz; Owner: postgres
--

GRANT ALL ON FUNCTION blitz.ellipse(center public.geometry, major numeric, minor numeric, angle numeric, numvertices integer) TO kugelblitz;


--
-- Name: FUNCTION ellipse_userid(_userid integer, _zeit1 integer, _zeit2 integer, _co_left numeric, _co_right numeric, _co_top numeric, _co_bottom numeric); Type: ACL; Schema: blitz; Owner: postgres
--

GRANT ALL ON FUNCTION blitz.ellipse_userid(_userid integer, _zeit1 integer, _zeit2 integer, _co_left numeric, _co_right numeric, _co_top numeric, _co_bottom numeric) TO kugelblitz;


--
-- Name: TABLE blitze; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze TO blitze;
GRANT SELECT ON TABLE blitz.blitze TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze TO blitzsammler;


--
-- Name: TABLE blitze1992; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze1992 TO blitze;
GRANT SELECT ON TABLE blitz.blitze1992 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze1992 TO blitzsammler;


--
-- Name: TABLE blitze1993; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze1993 TO blitze;
GRANT SELECT ON TABLE blitz.blitze1993 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze1993 TO blitzsammler;


--
-- Name: TABLE blitze1994; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze1994 TO blitze;
GRANT SELECT ON TABLE blitz.blitze1994 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze1994 TO blitzsammler;


--
-- Name: TABLE blitze1995; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze1995 TO blitze;
GRANT SELECT ON TABLE blitz.blitze1995 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze1995 TO blitzsammler;


--
-- Name: TABLE blitze1996; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze1996 TO blitze;
GRANT SELECT ON TABLE blitz.blitze1996 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze1996 TO blitzsammler;


--
-- Name: TABLE blitze1997; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze1997 TO blitze;
GRANT SELECT ON TABLE blitz.blitze1997 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze1997 TO blitzsammler;


--
-- Name: TABLE blitze1998; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze1998 TO blitze;
GRANT SELECT ON TABLE blitz.blitze1998 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze1998 TO blitzsammler;


--
-- Name: TABLE blitze1999; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze1999 TO blitze;
GRANT SELECT ON TABLE blitz.blitze1999 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze1999 TO blitzsammler;


--
-- Name: TABLE blitze2000; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2000 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2000 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2000 TO blitzsammler;


--
-- Name: TABLE blitze2001; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2001 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2001 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2001 TO blitzsammler;


--
-- Name: TABLE blitze2002; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2002 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2002 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2002 TO blitzsammler;


--
-- Name: TABLE blitze2003; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2003 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2003 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2003 TO blitzsammler;


--
-- Name: TABLE blitze2004; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2004 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2004 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2004 TO blitzsammler;


--
-- Name: TABLE blitze2005; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2005 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2005 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2005 TO blitzsammler;


--
-- Name: TABLE blitze2006; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2006 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2006 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2006 TO blitzsammler;


--
-- Name: TABLE blitze2007; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2007 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2007 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2007 TO blitzsammler;


--
-- Name: TABLE blitze2008; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2008 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2008 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2008 TO blitzsammler;


--
-- Name: TABLE blitze2009; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2009 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2009 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2009 TO blitzsammler;


--
-- Name: TABLE blitze2010; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2010 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2010 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2010 TO blitzsammler;


--
-- Name: TABLE blitze2011; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2011 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2011 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2011 TO blitzsammler;


--
-- Name: TABLE blitze2012; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2012 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2012 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2012 TO blitzsammler;


--
-- Name: TABLE blitze2013; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2013 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2013 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2013 TO blitzsammler;


--
-- Name: TABLE blitze2014; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2014 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2014 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2014 TO blitzsammler;


--
-- Name: TABLE blitze2015; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2015 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2015 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2015 TO blitzsammler;


--
-- Name: TABLE blitze2016; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2016 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2016 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2016 TO blitzsammler;


--
-- Name: TABLE blitze2017; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2017 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2017 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2017 TO blitzsammler;


--
-- Name: TABLE blitze2018; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2018 TO blitze;
GRANT SELECT ON TABLE blitz.blitze2018 TO geoserver;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2018 TO blitzsammler;


--
-- Name: TABLE blitze2019; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.blitze2019 TO blitze;
GRANT INSERT,UPDATE ON TABLE blitz.blitze2019 TO blitzsammler;
GRANT SELECT ON TABLE blitz.blitze2019 TO geoserver;


--
-- Name: SEQUENCE blitze_id_seq; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT,UPDATE ON SEQUENCE blitz.blitze_id_seq TO blitzsammler;


--
-- Name: TABLE vergleich; Type: ACL; Schema: blitz; Owner: postgres
--

GRANT SELECT ON TABLE blitz.vergleich TO blitze;


--
-- PostgreSQL database dump complete
--

