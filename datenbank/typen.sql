CREATE TYPE public.livegebiet AS
(
	id integer,
	gebiet character varying,
	gebietgeom geometry,
	blitzanzahl integer,
	alarm boolean,
	aktiv boolean
);

ALTER TYPE public.livegebiet
    OWNER TO postgres;

CREATE TYPE public.liveblitz AS
(
	id integer,
	zeit timestamp with time zone,
	typ integer,
	strom numeric,
	koordinate geometry,
	tlp integer,
	unixzeit integer
);

ALTER TYPE public.liveblitz
    OWNER TO postgres;
