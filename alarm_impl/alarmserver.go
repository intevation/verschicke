// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// BLIDS alarmserver.

package alarmserver

import (
	"database/sql"
	"encoding/base64"
	"errors"
	"log"
	"os"
	"strings"
	"time"

	"github.com/lib/pq"
)

type AlarmServer struct {
	conf     *config
	listener *pq.Listener
	db       *sql.DB
	stmts    *asStmts
}

var dsnReplacer = strings.NewReplacer(
	"u=", "user=",
	"pw=", "password=",
	"h=", "host=",
	"d=", "dbname=",
	"p=", "port=",
	"s=", "sslmode=")

// DSN returns the data soure name from the environment.
// If there is a configuration problem an error is returned.
func DSN() (string, error) {
	// TODO more sophistcated way to configure database connection.
	conn := os.Getenv("DB_CONNECTION")
	if conn == "" {
		return "", errors.New("Missing DB_CONNECTION env variable")
	}
	// Env vars in beanstalk are not allowed to have special chars
	// so the hole string is encoded in Base64.
	dec, err := base64.StdEncoding.DecodeString(conn)
	if err == nil {
		conn = string(dec)
	}
	return dsnReplacer.Replace(conn), nil
}

func NewAlarmServer() (*AlarmServer, error) {

	var as AlarmServer
	var err error

	notifyChannel := os.Getenv("NOTIFY_CHANNEL")
	if notifyChannel == "" {
		return nil, errors.New("Missing NOTIFY_CHANNEL env variable")
	}

	dsn, err := DSN()
	if err != nil {
		log.Fatalf("DSN problem: %v\n", err)
	}

	log.Println("pgconn " + dsn)

	as.db, err = sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}

	as.prepareStatements()

	err = as.readConfigFromDB()
	if err != nil {
		return nil, err
	}

	minReconn := time.Second
	maxReconn := 10 * time.Second
	as.listener = pq.NewListener(dsn, minReconn,
		maxReconn, handleConnError)
	err = as.listener.Listen(notifyChannel)
	if err != nil {
		return nil, err
	}

	return &as, nil
}
