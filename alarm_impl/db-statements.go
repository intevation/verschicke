// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// BLIDS alarmserver.

package alarmserver

import (
	"database/sql"
	"log"
)

type asStmts struct {
	getAlarmMsg     *sql.Stmt
	updateMsgStatus *sql.Stmt
}

func (as *AlarmServer) prepareStmt(stmt string) *sql.Stmt {
	s, err := as.db.Prepare(stmt)
	if err != nil {
		log.Panicf("Preparing statement '%s' failed: %s", stmt, err)
	}
	return s
}

func (as *AlarmServer) prepareStatements() {
	as.stmts = &asStmts{
		getAlarmMsg: as.prepareStmt("SELECT alarmserver.get_alarm_msg($1)"),
		updateMsgStatus: as.prepareStmt("UPDATE alarmserver.meldungen " +
			"SET erledigt = now(), status = $1, status_info = $2 " +
			"WHERE id = $3"),
	}
}
