module bitbucket.org/intevation/verschicke

go 1.13

require (
	bitbucket.org/intevation/kugelblitz v0.0.0-20190814092204-80e7b4f43b61
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/lib/pq v1.2.0
	golang.org/x/net v0.0.0-20191206103017-1ddd1de85cb0 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
