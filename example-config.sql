ALTER TYPE alarmserver.conf_option ADD VALUE 'MQTTHost';
ALTER TYPE alarmserver.conf_option ADD VALUE 'MQTTPort';
ALTER TYPE alarmserver.conf_option ADD VALUE 'MQTTUsername';
ALTER TYPE alarmserver.conf_option ADD VALUE 'MQTTPassword';
ALTER TYPE alarmserver.conf_option ADD VALUE 'MQTTUseSSL';
ALTER TYPE alarmserver.conf_option ADD VALUE 'MQTTCertPath';

COPY alarmserver.config(conf_option,string,int,bool) FROM STDIN WITH ( FORMAT CSV );
Debug,,,false
SmtpHost,mail.example.com,,
SmtpPort,,465,
SmtpHelo,alarmserver.example.com,,
SmtpUser,testuser,,
SmtpPasswd,t0p5ecRET,,
MailFrom,foo@example.com,,
EcallUser,user01,,
EcallPasswd,1234,,
EcallVoiceFrom,Blids,,
EcallFaxFrom,BLIDS 0049 XXX XXX XXX XXX,,
MQTTHost,mqtthost,,
MQTTPort,,1234,
MQTTUsername,foo,,
MQTTPassword,bar,,
MQTTUseSSL,,,false
MQTTCertPath,/mycert,,
\.
