# verschicke

## Build via Bitbucket Pipelines

If you create and push a tag to Bitbucket this will build and deploy the build artifact
to Bitbucket Downloads:

https://bitbucket.org/intevation/verschicke/downloads/

Note:

By default, the `git push` command doesn’t transfer tags to remote servers.
You will have to explicitly push tags to a shared server after you have
created them. This process is just like sharing remote branches — you can run
`git push origin <tagname>`.

The generated zip file can be deployed directly at AWS ElasticBeanstalk.

## Build manually

```shell
go build -o bin/alarmserver -tags mqtt
```

## Creating and deploying verschicke on AWS Elastic Beanstalk

```shell
go build -o bin/alarmserver -tags mqtt
zip -r alarmserver-$(git describe --always).zip Procfile bin/
```

Further informations:

- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/go-environment.html#go-simple-apps
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/go-procfile.html