// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.
//
// Author: Sascha Wilde <wilde@intevation.de>

// BLIDS alarmserver.

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/intevation/verschicke/alarm_impl"
	"bitbucket.org/intevation/kugelblitz/common"
)

func main() {
	var (
		version bool
	)
	log.Println("Alarmserver RESTART")
	// Webserver for AWS EBS
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)
	})
	go func() { log.Fatal(http.ListenAndServe(":5000", nil)) }()

	flag.BoolVar(&version, "version", false,
		"Print version and exit.")

	flag.Parse()

	if version {
		common.PrintVersionAndExit()
	}

	as, err := alarmserver.NewAlarmServer()
	if err != nil {
		log.Fatal(err)
	}

	as.Debug("Alarmserver starting to listen for notifications...")

	for {
		as.ProcessNotifications()
	}
}
